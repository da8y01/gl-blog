---
title: Trilogía Cósmica - C. S. Lewis
date: "2024-01-08T16:14:37.121Z"
description: "La serie Trilogía Cósmica del autor inglés Clive Staples Lewis."
tags: libro literatura TrilogíaCósmica CSLewis Lewis
---


![CSLewis_TrilogiaCosmica_Frente](./CSLewis_TrilogiaCosmica_Frente.jpg)
![CSLewis_TrilogiaCosmica_Respaldo](./CSLewis_TrilogiaCosmica_Respaldo.jpg)


## <span id="PlanetaSilencioso">[Más allá del planeta silencioso](#PlanetaSilencioso)</span>
![CSLewis_1-PlanetaSilencioso_Portada](./CSLewis_1-PlanetaSilencioso_Portada.jpg)
![CSLewis_1-PlanetaSilencioso_Contraportada](./CSLewis_1-PlanetaSilencioso_Contraportada.jpg)

### 1
Se presenta al doctor Elwin Ransom y se describe que va como caminante/explorador por los páramos salvajes de Sterk y Nodderby. Se narra el encuentro con la pariente del muchacho empleado de La Colina, Harry. Se encuentran los viejos amigos de colegio Ransom y Devine junto con Weston. Explican lo que están haciendo con Harry. Acogen por esa noche a Ransom.

### 2
Se cuenta cómo al aceptar el trago ofrecido por Devine, Ransom cae hipnotizado/drogado y de cómo lo llevan forzado a algún lugar oscuro.

### 3
Ransom despierta en el cuarto oscuro al que fue trasladado, y después de examinar su estado y su entorno, se da cuenta que está en una nave/aeronave. Al final entra Weston y le informa que el objeto celeste grande que había estado observando es la Tierra.

### 4
Ransom y Weston hablan acerca de la situación de Ransom, se describe la nave y desayunan.

### 5
Se describe el pasar de los días en la nave, de los turnos para vigilar a Ransom, se habla del espacio, de cómo se convirtió en "mayordomo", de la conversación que escucha Ransom y de las cosas que le esperan (sorns, sacrificios humanos, repugnantes monstruos sin sexo, ...).

### 6
Se describen los cambios en el viaje y en la nave debidos a la proximidad de Malacandra y su campo gravitacional. Se describen los cambios en el estilo de vida dentro de la nave y los trabajos que hay que hacer para adaptarse al entorno próximo. Entran en la atmósfera y se percibe el cambio de entorno.

### 7
Llegan a Malacandra, descargan el cargamento en la casa de Weston (científico) y Devine, almuerzan. Al terminar de almorzar, los dos captores tratan de entregar al Dr. Ransom a seis seres que están al otro lado de la orilla estrecha en la que se encuentran; finalmente Ransom se escabuye y huye.

### 8
Ransom huye por el bosque, al final se duerme en un arroyo cálido.

### 9
Ransom despierta y continúa su travesía por el bosque soportando ciertas alucinanciones; bebe el agua con sabor mineral y caliente de los arroyos de Malacandra, se encuentra con los animales que parecen jirafas, observa las montañas alargadas y picudas de Malacandra; ve un sorn por el bosque y huye para al final encontrarse con el jross.

### 10
El jross invita a Ransom al bote, navegan por el jandramit y llegan al poblado de los jrossa donde pasan la noche. El jross explica algunos aspectos de Malacandra (como acerca de jaranda y jandramit).

### 11
Ransom vive y convive con los jrossa, aprende de su comunidad u cultura, conversan e intercambian información (de las especies de Malacandra, del planeta (tierra/jandra) natal Thulcandra, de la historia/creencias de los puebos, ...).

### 12
Jyoi y Ransom trabajan en el bote y dialogan (acerca de la guerra, los placeres, la naturaleza, el instinto, de los depredadores/enemigos/acechantes y de los eldil).

### 13
Se ultiman los preparativos para la cacería de jnakra (monstruo con grandes fauces y dientes chasqueantes como tiburón), Jyoi, Wjin y Ransom eligen la 2ª estrategia (la de adelantarse), se encuentran con un eldil que les da instrucciones de enviar al jombre donde Oyarsa, deciden hacerlo después de cazar al jnakra, lo cazan exitosamente, mientras se recuperan y celebran, un riflazo mata a Jyoi, Wjin y Ransom se cubren, dialogan y acuerdan seguir inmediatamente las indicaciones del eldil, así que Wjin da instrucciones a Ransom para llegar donde Oyarsa, y Ransom parte.

### 14
Ransom se adentra en el bosque buscando el camino que lo llevaría a la torre de Augray, finalmente encuentra el camino, lo recorre y llega a una caverna iluminada por una hoguera y con una altísima cámara de piedra verde y un sorn.

### 15
Augray el sorn lo recibe en su caverna, le convida oxígeno (benéfico para los jrossa y para él también), le da comida vegetal común y leche de los animales que parecen jirafas en los cuales los sorns trabajan como pastores; hablan acerca de los jrossa, los eldila, Oyarsa, se describen aspectos y características de éstos, finalmente el sorn Augray le enseña a Ransom su planeta/jandra (Tierra/Thulcandra) a través de una ventana y Ransom la reconoce.

### 16
Al día siguiente acuerdan que Augray llevará a Ransom a Oyarsa, le explica que los pfifltriggi construyen cosas, emprenden el camino con Ransom en el hombro de Augray, lo provee de oxígeno, se describe el entrono por el que transitan así como los antiguos bosque de Malacandra, ven uno que otro sorn en el camino hasta que llegan a la caverna (sistema de excavaciones) de un científico con sus ayudantes/aprendices, y finalmente la conversación se extiende acerca de la Tierra.

### 17
Augray y Ransom siguen su camino, Ransom en el hombro de Augray, ven la arena levantada en los desiertos por el viento, siguen el viaje hasta llegar a un declive que parecia un precipicio, por el que bajaron para desembocar en un nuevo y hermoso jandramit que era Meldilorn, se describe el lugar, con el gong llaman una embarcación manejada por un jross que llevaría a Ransom a la isla principal de Meldilorn, entretanto Augray y Ransom se despiden, le ofrece el reloj a Augray pero éste le recomienda obsequiarlo a un pfifltriggi, el joven jross le explica lo que hará mientras se encuentra con Oyarsa, se describe la isla (los eldila, el tránsito de jnau, ...), finalmente Ransom se encuentra a sí mismo en la avenida de los monolitos y observa que en ellos hay tallada y representada la historia del sistema solar y se encuentra con Kanakaberaka, el pfifltriggi, quien esculpe una nueva escena en un monolito, retratándolo a él y a su raza, hablan del lenguaje y las lenguas y Kanakaberaka le describe su mundo y lo convida allí y hablan del trabajo.

### 18
Ransom es acogido en la casa de huéspedes donde comparte con los demás jnau, al día siguiente es convocado donde Oyarsa, se dirige a la avenida de monolitos en la cima de Meldilorn (donde hay muchos otros jnau en dos hileras), y allí está presente la asamblea que lo escuchará. Habla con Oyarsa (acerca del miedo, de su traslado desde Thulcandra, de Thulcandra y de las dos preguntas que formuló Oyarsa), al final la conversación se ve interrumpida por un grupo grande de jrossa que transportan algo.

### 19
Los jrossa traen a los prisioneros Weston y Devine, Ransom los alcanza a ver con ojos malacándricos, Oyarsa los interroga, Weston y Devine piensan que es algún truco o hechicería, Weston ejecuta su espectáculo, causa gracia entre los jnau presentes en la asamblea, dan explicaciones a medias, Oyarsa ordena que retiren a Weston para que se serene, le rinden honores fúnebres a los jrossa muertos.

### 20
Weston y Oyarsa dialogan, Ransom sirve de intérprete, Weston piensa que lo van a matar y habla un discurso (acerca de la lealtad de un hombre por la humanidad), se plantean cuestiones ideológicas, al final Oyarsa dictamina que les da un máximo de 90 días para llegar a Thulcandra, tiempo después del cual la nave se descorporizará convirtiéndose en nada.

### 21
Ransom habla con Oyarsa y Oyarsa se despide, Weston, Devine y Ransom se embarcan en la nave hacia la Tierra, se aprecia el alejamiento de Malacandra, los días pasan en la nave hasta que se acercan a la Tierra, al final logran llegar, Ransom se despierta solo en la nave en una noche lluviosa, abandona la nave y ésta se descorporiza mientras Ransom se dirige a la civilización de nuevo.

### 22
Se cuenta como el profesor "Ransom" toma su aventura como alucinaciones producto de la enfermedad en la que cayó, hasta el momento de la particular coincidencia con el colega que tenía interés en la palabra "Oyarses". Se hacen estudios y se recopilan datos y hechos, concernientes a estos temas (los planetas, Marte, los platónicos medievales, "Weston", ...) y deciden contar la historia en forma de ficción.

### POST ESCRIPTUM
"Ransom" escribe a Lewis acerca de observaciones hechas a propósito del manuscrito final de la historia.


## <span id="Perelandra">[Perelandra](#Perelandra)</span>
![CSLewis_2-Perelandra_Portada](./CSLewis_2-Perelandra_Portada.jpg)
![CSLewis_2-Perelandra_Contraportada](./CSLewis_2-Perelandra_Contraportada.jpg)

### 1
El autor, CS Lewis, narra cómo acude a la casa de Elwin Ransom debido a un telegrama enviado por éste. El autor describe el miedo y sensación de demencia que lo invade al acercarse a la casa y en vista de los acontecimientos y hechos sucedidos, experimentados y vividos. Al final llega a la casa de Ransom, se encuentra con un eldil y luego con Ransom.

### 2
Elwin Ransom y CS Lewis hablan acerca de los próximos acontecimientos, de la preparación para éstos y del rol que le corresponde a cada uno. Hablan acerca de la misión y el papel de Ransom, de los acontecimientos cambiantes, del principio del fin del aislamiento de Thulcandra, del posible ataque a Perelandra por parte del arconte negro (nuestro propio Oyarsa torcido), de que Ransom fue elegido para estar allá, de los idiomas, de las especulaciones en cuanto al movimiento de Venus, hablan acerca del papel de CS Lewis.
Se cuenta como CS Lewis ayuda a partir a Ransom y cómo al cabo de un año largo regresa, y es recibido por CS Lewis y Humprey (el médico), de cómo llega mucho más saludable, rejuvenecido y vital, y de cómo contesta algunas preguntas mientras se vuelve a instalar.

### 3
Descripción vaga de cómo es viajar en un ataúd, del descenso sobre Perelandra, de cómo cae en el océano, se describe el paisaje, se describe la tormenta, de cómo logra alcanzar una isla flotante, se describen las islas flotantes, de cómo aprende a caminar en las islas, de la región de los bosques, de los frutos, del ocaso, de la noche, la oscuridad y la calidez.

### 4
Ransom se despierta en la isla con un dragón, descubre nuevas particularidades de la isla (árboles, burbuja, comida).
Se congregan los animales en una isla en la que desembarca una figura humana que resulta ser una mujer, la Dama Verde.
Ransom trata de establecer contacto y alcanzarla.

### 5
Ransom y la Dama hablan.
Hablan del conocimiento que tiene la Dama de los otros mundos, hablan de que Thulcandra es un recodo en el tiempo, de las formas de las criaturas, de que la forma humana será la de las especies en los mundos en adelante.
Hablan del Rey, de que ellos son los únicos, de la concepción de la muerte, de la concepción de no tener lo que se desea sino otra cosa, y de cómo los corazones pueden tomar con distinto ánimo lo que les es concedido.

### 6
Ransom y la Dama cabalgando en peces plateados desembarcan en la Tierra Fija. Entretanto un objeto venido del Cielo Profundo a caído al mar; cuando desde la Tierra Fija Ransom logra observar el objeto, se da cuenta que es Weston. Al final la Dama y Ransom se dirigen a la bahía a donde se dirige Weston para encontrarse con él y hacerle frente.

### 7
Ransom y la Dama se encuentran con Weston, la Dama parte y Ransom se queda amenazado por Weston. Ransom y Weston hablan. Hablan acerca de la concepción utilitaria y egoísta del conocimiento/ciencia/Weston, de la influencia de la biología en Weston durante su convalecencia, de la nueva concepción espiritual (del todo), de que Weston ha sido guiado/elegido, acerca del dualismo en el universo, de la conducción de Weston. Al final Weston sufre un ataque y cae la noche en Tierra Fija.

### 8
Ransom se despierta y no encuentra rastros de Weston. Después mientras se refresca en el mar ve un pez dorado, lo cabalga, se alejan rápidamente de Tierra Fija, cae la noche, conoce el mundo lumínico con figuras humanas abajo de la superficie del mar, y finalmente desembarca en una isla.
En la isla come y duerme, pero aún de noche es despertado por una conversación entre un hombre y una mujer, al parecer son la Dama y Weston.
Al final de la conversación Ransom siente algo parecido a triunfo/victoria/fiesta en el ambiente, se vuelve a dormir.

### 9
Ransom despierta, ve las ranas degolladas por "Weston", y al verlo a él se desmaya. Despierta y los encuentra hablando, Ransom "incursiona" en la conversación. La conversación se extiende, la criatura está tentando a la Dama (principalmente con el asunto de la Tierra Fija) y Ransom juega su rol de evitar que la Dama sea tentada. Al final la Dama decide dormir, la criatura se sienta a su lado y empieza el tormento de la criatura hacia Ransom, el cual resiste, y cae la noche.

### 10
La lucha sigue, el Tentador intenta formar una imagen de historias trágicas de mujeres en la mente de la Dama, el Tentador pretende formar una idea general de "deber" e independencia/autonomía, de rebelión necesaria contra la órden impuesta por Maleldil, las ideas de teatralidad, egoísmo y una falta de consideración puramente intelectual parecen pilares de la estrategia y crecen en la Dama, al final con la escena de la ropa y el espejo entra el juego el concepto de vanidad, además de la apreciación y concepción diferentes de sí misma.

### 11
Ransom debate consigo mismo, cuestiona la divinidad, del destino de mundos nuevos como Perelandra; al final se da cuenta que debe ejecutar una acción física, matar a Weston.

### 12
Ransom inicia la pelea con el Antihombre, él se especializa en boxeo, el otro en uñas y dientes; realmente el Antihombre no tiene ventaja/fuerza sobrenatural o diabólica, simplemente físicamente es Weston. Después de una arremetida de Ransom el Antihombre escapa hasta extender la presecución a alta mar cabalgando peces.

### 13
En medio de la oscuridad y de personas acuáticas cabalga Ransom, y tiene la idea de que tal vez las islas sean como nubes para un mundo sub-acuático.
La persecución no se podría extender por mucho más tiempo y finalmente se encuentra con Weston y hablan acerca de la vida y de la naturaleza de cáscara.
Al final oyen sonido de rompientes y Ransom es arrancado del pez hacia las profundidades del mar.

### 14
Ransom se encuentra en una playa de guijarros, forcejea con el Antihombre hasta quitarle el aliento, se cansa de esperar y al explorar descubre que se encuentra en una caverna. El recorrido exploratorio tratando de hallar una salida lo lleva a una cueva que tiene un risco/precipicio con un brillo cegador, un mar de fuego. Al llegar allí es seguido por el Antihombre y un animal tripartito con antenas, ojos y patas. Ransom arremete contra el Antihombre, noqueándolo con una roca, al intentar enfrentar la otra criatura, se da cuenta que es dócil, ésta parte por donde vino. Finalmente Ransom arroja el cuerpo de Weston al abismo, se siente exhausto y duerme.

### 15
Ransom recorre las cavernas y ve cosas fantásticas hasta que cae en una corriente rápida que lo expulsa a un charco en un pico donde descansa y se recupera; escribe la inscripción. Algún tiempo después parte, se describen los parajes del recorrido, hasta que llega a un valle de flores con un lago rodeado de picos en el que hay un ataúd blanco abierto y dos eldila.

### 16
Ransom y los eldila hablan, Perelandra le da a entender que toda esa creación que había sido mantenida y resguardada por él va a ser entregada a nuevos amos.
Los eldila adoptan forma de humanos gigantes blancos con un halo distintivo para aparecer agradables ante el Rey la Reina.
De cómo en medio de una celebración de criaturas y animales se prepara la entrada ceremonial del Rey la Reina al valle florido cubierto por una luz sagrada.

### 17
De la asamblea. La pareja Paraíso/el Rey y la Reina/Tor y Tinidril reciben la creación y deciden que el eldil Perelandra los acompañe en la labor. De las dos órdenes de Tor. De los planes de Tor, del comienzo, de la reivindicación. De la alabanza a Él y la Gran Danza. Del tiempo que pasó, de la despedida y la partida.


## <span id="HorribleFortaleza">[Esa horrible fortaleza](#HorribleFortaleza)</span>
![CSLewis_3-HorribleFortaleza_Portada](./CSLewis_3-HorribleFortaleza_Portada.jpg)
![CSLewis_3-HorribleFortaleza_Contraportada](./CSLewis_3-HorribleFortaleza_Contraportada.jpg)

### *Prefacio*
Acerca de la naturaleza/género de la historia, de referncias y del marco de la historia.

### 1. Venta de propiedades del *college*
Se presenta a Jane Studdock, aburrida y solitaria en la casa, del sueño/pesadilla que tuvo, sale a dar una vuelta.
Se presenta a Mark Studdock, cuando se dirige a la asamblea del college Brancton se encuentra con Curry, hablan acerca de la asamblea, hablan de Lord Feverstone (Dick Devine), entran a Bristol a tomar un trago.
El autor narra cómo es el college Brancton y el bosque Brangdon, y habla del pozo y de historia.
Se ejecuta la asamblea donde se responden cartas y se vende el bosque Brangdon al NICE.
Jane se encuentra con la señora Dimble y Cecil Dimble, almuerzan, hablan acerca de la situación del college, de los asuntos que se están tratando, hablan de la leyenda del Rey Arturo, del sueño de Jane.

### 2. Cena con el Vicerrector
Curry (Vicerrector), James Busby (Tesorero), lord Feverstone (Dick Devine) y Mark Studdock cenan; hablan del NICE, de la humanidad y la ciencia, de los tres problemas, del trabajo de Mark como sociólogo y de ir a ver a John Wither quien realmente sabe de las actividades del NICE detrás del director Jules.
Mark vuelve a casa y se encuentra con Jane una acongojada. En la mañana discuten, Mark se va con Feverstone a ver a Wither y Jane decide acudir donde la señorita Ironwood.
Mark y Feverstone llegan a Belbury en coche, Jane llega a St. Anne en tren.

### 3. Belbury y St. Anne's-on-the-Hill
En una recargada mansión eduardiana en Belbury, Mark, en compañía de Feverstone, se entrevista con John Wither, el DD (Director Delegado). A continuación Mark almuerza y después entabla relación con algunos miembros del NICE reclutados allí.
Entretanto Camilla Denniston conduce a Jane ante la señorita Ironwood, quien le diagnostica que es vidente y trata de convencerla de que se una a su bando.
Mark se encuentra hablando con "Hada" Hardcastle, hablan aspectos relacionados con el trabajo, del Papeleo. En la cena William Hingest (Bill el Tormentas, miembro de Brancton) le cuenta que parte, y le da recomendaciones y avisos a Mark.
Al final Jane llega al apartamento.

### 4. La liquidación de los anacronismos
Margareet Dimble llega a pasar la noche en el apartamento de Jane Studdock (Jane Tudor) debido a que la constrcción del NICE en el bosque Brangdon y la rivera del río Wynd ha comenzado. Jane tiene una pesadilla en la noche, se la cuenta a la señora Dimble.
En Belbury, Mark habla con el Párroco Loco, Straik, del que le había hablado Bill el Tormentas (William Heingest), hablan acerca de que la ciencia es un instrumento irresistible, del juicio y la resurrección aquí y ahora, y del papel que jugarán o del lado del que estarán. Luego en la reunión de la comisión de Belbury se habla acerca de los trabajos que ya habían empezado en Edgestow y se da noticia del asesinato de William Heingest.
Al día siguiente cuando Margaret y Jane salen, Jane se encuentra con Curry quien le informa del asesinato de Heingest, Jane se da cuenta que de nuevo su sueño fue una videncia.
Cosser y Mark hablan y ejecutan el trabajo que tienen de hacer el informe respecto a la aldea Cure Hardy, visitan el pueblo, de regreso Mark se queda en Edgestow y pasa la noche en casa con Jane, se reencuentran.
Esa noche mientras miembros del Bracton en la Sala Comunitaria cenan, Curry y Feverstone hablan de reemplazar las dos nuevas vacantes del college, entretanto algo pasa en los trabajos de afuera que quiebra la ventana.

### 5. Flexibilidad
Mark regresa a Belbury, trata de hablar y aclarar su situación con Steele y Cosser, también con John Wither (el DD) sin éxito; fija una cita para la próxima mañana, habla con Hada Hardcastle, hablan de su trabajo real, de Alcasan; Mark termina el día deambulando y explora una parte de la mansión con animales, parte de la vivisección del NICE.
A la mañana siguiente Mark se entrevista con el DD; recibe una carta de Brancton, la responde; al final Mark y Feverstone discuten, hablan acerca de la insinuación de renunciar a la beca hecha por Feverstone a G. C. Curry (vicerrector de Bracton) a causa de que Hada Hardcastle le había dicho que Mark ya no abandonaría NICE, hablan acerca de que Feverstone interceda por Mark ante Curry con respecto a la beca de investigación.
Entretanto Jane es abordada por Camilla Denniston y Arthur Denniston, le hablan del Rey Pescador y de la Reina Pescadora, del Sura, y en general le piden que se una a su bando, que use sus dotes de clarividencia en su bando.

### 6. Niebla
Mark asiste a la fija citada con John Wither (el DD) y le lleva el formulario diligenciado, el DD muestra una actitud diferente, más displiciente, pero finalmente llegan a un acuerdo mucho menos generoso del que se le había propuesto al principio.
Los trabajos del NICE en las propiedades compradas al college Bracton continúan, "invaden" la franja sobre el río Wynd y pequeños altercados en Edgestow pasan desapercibidos.
Mark trabaja en el caso de la rehabilitación de Alcasan, Hada Hardcastle lo pone en contacto con el capitán O'Hara para tal labor.
Se llevan a cabo las exequias de Hingest.
Mark obtiene acceso a la biblioteca, es aceptado por el selecto grupo que allí se reúne, y lo hacen partícipe de sus tertulias; es allí donde se le asigna la tarea de escribir los artículos "criminales" acerca de los disturbios en Edgestow.
Jane sueña con un hombre, se topa con él en la realidad, y a continuación se dirige a St. Anne's donde los Denniston.

### 7. El Pendragón
Jane llega a El Portal en St. Anne's y se entrevista con el director (Rey Pescador). Hablan acerca de Mark, lo que implica, y las alternativas de acción a tomar.
Cuando Jane regresa en tren a Edgestow es capturada por Hada Hardcastle quien la interroga y maltrara; las circunstancias hacen que los captores tengan que escaparse y a ella la sueltan.
Jane es socorrida por una pareja en un coche que la llevan a St. Anne's.

### 8. Claro de Luna en Belbury
Hada Hardcastle y el DD (Wither) hablan acerca de la detención de Jane Studdock. Van donde la "cabeza".
Jane despierta en St. Anne's, conoce al oso Bultitude y a MacPhee. El señor Dimble y MacPhee son solicitados por el director.
En Belbury el almuerzo transcurre feliz, se habla acerca del tumulto, las reacciones, los artículos y las medidas a tomar, de (Dick) Feverstone como gobernador. El DD le ofrece/recomienda a Mark traer a su esposa Jane, Mark lo rechaza, el Hada lo critica por esto.
Durante la cena Filostrato expone su teoría de la higiene y la vida orgánica; después de la cena hablan Filostrato y Mark y al final Straik (el sacerdote) en una habitación a la luz de la luna. Hablan del modelo que es la luna y de crear a Dios, y de Alcasan como primer bosquejo de él.
Al final van a entrevistarse con la "cabeza".

### 9. La cabeza del sarraceno
Jane (Studdock) en el Cuarto Azul, en presencia del director (Rey Pescador) y otros narra el sueño que tuvo respecto a la visita a la "cabeza" por parte de Filostrato, Straik y Mark.
Mark se despierta adolorido por la experiencia de la noche anterior y con la mentalidad de traer a su esposa Jane; en el desayuno el Hada Hardcastle lo aborda. Mark va a ver al DD (Wither) pero no logra establecer conversación. Aterrado decide irse, cuando está cerca de la salida ve al DD, se arrepiente y se devuelve.
MacPhee y Jane hablan acerca de la historia del director (Ransom) y sus viajes, después Camilla (Dennistone) y Jane charlan.
Acontece una reunión general en el Cuarto Azul, se discute de la situación del enemigo, de la "cabeza del sarraceno" y de su propia posición; se concluye que la acción más peligrosa es la del bosque Brangdon.
Se analizan las implicaciones e intenciones del enemigo, del cuerpo de Merlin, de que la época adecuada ha llegado, del uso no sólo de las formas de poder modernas o materialistas, sino también de energía y conocimiento eldílico.

### 10. La ciudad conquistada
Mark (Studdock) es acusado por el Hada (Hardcastle) y John (Wither) por el asesinato de Hingest (Bill el Tormentas) teniendo como prueba acusatoria la billetera extraviada de Mark; al final lo persuaden de que este caso quede dentro del instituto (NICE), y le insisten en que traiga a su esposa Jane (Studdock).
Al salir de la oficina del DD, Mark se dirige a Edgestow sin avisar, al llegar a la casa deshabilitada decide ir a donde los Dimble, pasando primero por Bristol.
Dimble y Mark hablan, no llegan a ningún acuerdo concreto, a la salida de Mark lo arrestan por el asesinato de William Hingest.
Dimble se reúne en St. Anne's con los demás, le cuentan la situación, del sueño de Jane (Studdock), de la tumba de Merlín, del túnel, del portón blanco y del plan a ejecutar esa misma noche.

### 11. Empezó la batalla
Jane (Studdock), (Cecil) Dimble y (Arthur) Denniston van camino al encuentro de Merlín, ven una fogata y se dirigen a ella.
En el despacho del DD, al principio con el Hada, Wither y Frost conversan, de la situación de Mark, y de cómo conseguir sus objetivos, de cómo tener a Jane por medio de Mark.
En la celda blanca de Belbury, Mark reflexiona sobre su vida, decisiones y acciones de las personas importantes en su vida; al final llega Frost.

### 12. Noche húmeda y ventosa
En el campamento de la fogata (Jane, Dimble y Denniston) no encuentran nada, siguen unas huellas que desaparecen en la hierba, se dirigen al camino para buscar un portón blanco.
Stone le reporta a Wither la situación de la expedición que busca a Merlín desde la cámara/cripta.
Jane, Dimble y Denniston deambulan cuando ven un caballo con un jinete, no logran alcanzarlo, se escapa.
Frost y Mark hablan, de los macrobios, del futuro de la raza humana, al final Frost es interrumpido con una nota y parte.
Mientras, en la cocina de St. Anne's pasan el tiempo hablando de las relaciones de los animales, llega un jinete.
Wither recibe en una habitación el cuerpo de alguien, llega Frost y juntos le dan de beber y comer, no presta atención a lo que pretenden decirle.
Mark en su celda reflexiona, de la tentación, se duerme.

### 13. Hicieron caer el Cielo Profundo sobre sus cabezas
El jinete (Merlín) habla con Ransom y le hace las tres preguntas.
En el cuarto externo Wither y Frost tienen problemas de comunicación con el visitante, acuerdan poner vigilancia.
Ransom y Merlín conversan vestidos con ropajes, los otros son espectadores.
Los señores Dimble hablan acerca de las costumbres/maneras/caracter de Merlín.
Ransom y Merlín hablan en el Cuarto Azul, hablan de fácticas/estrategias para abordar al enemigo, al final Ransom le hace entender que por el error de hacer caer el Cielo Profundo sobre sus cabezas podrán ser combatidos, y que Merlín debe obedecerlo.

### 14. "La verdadera vida es encuentro"
Frost lleva a Mark a un cuarto extraño, luego es llevado a vigilar al visitante en la cama.
La señora Dimble y Jane arreglan el Pabellón para los Maggs, de la visión/sueño/alucinación de Jane.
De la mente/comportamientos del señor Bultitude, y de cómo Sid y Len lo capturan.
Mark y el vagabundo intiman, el vagabundo pasa interrogatorios.
Ransom y Jane hablan, de la venida de los amos.
De la experiencia religiosa y del yo de Jane.

### 15. El descenso de los Dioses
Se reúnen los cinco Oyarsa en el Cuarto Azul, Merlín recibe los poderes.
Merlín, en presencia de Wither, Frost y Mark, se entrevista con el vagabundo.
Wither y Frost cumplen la orden de llevarle ropa al falso Merlín, discuten un plan y qué hacer.
Mark, en el Cuarto Objetivo, es sometido a la prueba de patear un crucifijo.
El director Jules es atendido y espera a Wither, Wither llega con su "séquito".

### 16. Banquete en Belbury
En el banquete las personas pierden la capacidad de hablar correctamente, se produce el caos, entran los animales, hay muertes.
Se explica que Merlín, después de lanzar la maldición de Babel, libera a los prisioneros y animales, entre éstos el señor (Tom) Maggs y el señor (Oso) Bultitude.
Mark es despertado por Merlín, le entrega una carta de (Arthur) Denniston y lo pone en camino hacia St. Anne's.
Wither, Filostrato y Straik van a la cámara de la cabeza; Filostrato es decapitado, Straik es apuñalado, Wither se encuentra con el señor Bultitude.
Feverstone espera y escapa, en el coche es manipulado. Frost prende fuego al Cuarto Objetivo con él adentro.

### 17. Venus en St. Anne's
Mark se dirige tímidamente a su reencuentro con Jane, se recupera en un hostal.
Las mujeres en el Guardarropa escogen vestidos para la noche.
Feverstone lucha por llegar a Edgestow.
Después de la cena todos hablan acerca de Logres, Gran Bretaña e Inglaterra.
Curry en tren trata de llegar a Edgestow.
Los animales y la excitación de Venus invaden St. Anne's.
Mientras Mark se dirige a St. Anne's piensa en Jane.
Jane, después de despedirse del director, se dirige al Pabellón para encontrarse con Mark.


## Glosario
* Lisonja. Pág. 286. Adulación, alabanza fingida.
* Ultra vires. Pág. 265. Beyond the powers.
* Prima facie. Pág. 266. A primera vista, Como primera impresión.
* Sine qua non. Pág. 268. Condición sin la cual no.
* Ethos. Pág. 268. Punto de partida, Aparecer, Inclinación, Personalidad.
* Dérmanche. Pág. 279. Movimiento.
* Marrullero/Marrullería. Pág. 315. Halago con astucia para engañar.
* Quevedos. Antecesores de las gafas.
* Palafrenero. Pág. 352. Criado que lleva el caballo por el freno.
* Cariátide. Pág. 420. Estatua de figura humana que sirve de columna o pilastra.
* Veleidad. Pág. 420. Voluntad antojadiza, deseo vano. Ligereza, inconsistencia.
* Afasia. Pág. 431. Pérdida de la palabra por trastorno cerebral.


## <span id="referencias">[REFERENCIAS](#referencias)</span>
* [https://es.wikipedia.org/wiki/Trilog%C3%ADa_c%C3%B3smica][1]
* [https://es.wikipedia.org/wiki/C._S._Lewis][2]


[1]: https://es.wikipedia.org/wiki/Trilog%C3%ADa_c%C3%B3smica
[2]: https://es.wikipedia.org/wiki/C._S._Lewis
