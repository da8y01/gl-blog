---
title: NES clips
date: "2023-07-20T12:15:37.121Z"
description: "Some NES emulator games clippings."
tags: NES NintendoEntertainmentSystem clips game games VideoGame VideoGames Nintendo emulator
image: ./NES_logo.svg.png
---


## <span id="CaptainTsubasaVol2">[Captain Tsubasa Vol. 2 Super Striker](#CaptainTsubasaVol2)</span>

![CaptainTsubasaVol2_Cup4_AsiaCup_ToGame1_Syria_2](./CaptainTsubasaVol2_Cup4_AsiaCup_ToGame1_Syria_2.png)
*Captain Tsubasa Vol2 - Cup 4 The Asia Cup - Game 1 Syria*

![CaptainTsubasaVol2_Cup4_AsiaCup_ToGame1_Syria_1](./CaptainTsubasaVol2_Cup4_AsiaCup_ToGame1_Syria_1.png)

![CaptainTsubasaVol2_28](./CaptainTsubasaVol2_28.png)

![CaptainTsubasaVol2_27](./CaptainTsubasaVol2_27.png)

![CaptainTsubasaVol2_26](./CaptainTsubasaVol2_26.png)

![CaptainTsubasaVol2_25](./CaptainTsubasaVol2_25.png)

![CaptainTsubasaVol2_24](./CaptainTsubasaVol2_24.png)

![CaptainTsubasaVol2_23](./CaptainTsubasaVol2_23.png)

![CaptainTsubasaVol2_22](./CaptainTsubasaVol2_22.png)

![CaptainTsubasaVol2_21](./CaptainTsubasaVol2_21.png)

![CaptainTsubasaVol2_20](./CaptainTsubasaVol2_20.png)

![CaptainTsubasaVol2_19](./CaptainTsubasaVol2_19.png)

![CaptainTsubasaVol2_18](./CaptainTsubasaVol2_18.png)

![CaptainTsubasaVol2_17](./CaptainTsubasaVol2_17.png)

![CaptainTsubasaVol2_16](./CaptainTsubasaVol2_16.png)

![CaptainTsubasaVol2_15](./CaptainTsubasaVol2_15.png)

![CaptainTsubasaVol2_14](./CaptainTsubasaVol2_14.png)

![CaptainTsubasaVol2_13](./CaptainTsubasaVol2_13.png)

![CaptainTsubasaVol2_12](./CaptainTsubasaVol2_12.png)

![CaptainTsubasaVol2_11](./CaptainTsubasaVol2_11.png)

![CaptainTsubasaVol2_10](./CaptainTsubasaVol2_10.png)

![CaptainTsubasaVol2_Cup3_JapanCup_ToGame4Finals_Japan_2](./CaptainTsubasaVol2_Cup3_JapanCup_ToGame4Finals_Japan_2.png)
*Captain Tsubasa Vol2 - Cup 3 Japan Cup - Finals Japan*

![CaptainTsubasaVol2_Cup3_JapanCup_ToGame4Finals_Japan_1](./CaptainTsubasaVol2_Cup3_JapanCup_ToGame4Finals_Japan_1.png)

![CaptainTsubasaVol2_09](./CaptainTsubasaVol2_09.png)

![CaptainTsubasaVol2_08](./CaptainTsubasaVol2_08.png)

![CaptainTsubasaVol2_07](./CaptainTsubasaVol2_07.png)

![CaptainTsubasaVol2_06](./CaptainTsubasaVol2_06.png)

![CaptainTsubasaVol2_Cup3_JapanCup_ToGame3_Hamburg](./CaptainTsubasaVol2_Cup3_JapanCup_ToGame3_Hamburg.png)
*Captain Tsubasa Vol2 - Cup 3 Japan Cup - Game 3 Hamburg*

![CaptainTsubasaVol2_05](./CaptainTsubasaVol2_05.png)

![CaptainTsubasaVol2_Cup3_JapanCup_ToGame2_Uruguay_2](./CaptainTsubasaVol2_Cup3_JapanCup_ToGame2_Uruguay_2.png)
*Captain Tsubasa Vol2 - Cup 3 Japan Cup - Game 2 Uruguay*

![CaptainTsubasaVol2_Cup3_JapanCup_ToGame2_Uruguay_1](./CaptainTsubasaVol2_Cup3_JapanCup_ToGame2_Uruguay_1.png)

![CaptainTsubasaVol2_Cup3_JapanCup_ToGame1_Rome_2](./CaptainTsubasaVol2_Cup3_JapanCup_ToGame1_Rome_2.png)
*Captain Tsubasa Vol2 - Cup 3 Japan Cup - Game 1 Rome*

![CaptainTsubasaVol2_Cup3_JapanCup_ToGame1_Rome_1](./CaptainTsubasaVol2_Cup3_JapanCup_ToGame1_Rome_1.png)

![CaptainTsubasaVol2_04](./CaptainTsubasaVol2_04.png)

![CaptainTsubasaVol2_03](./CaptainTsubasaVol2_03.png)

![CaptainTsubasaVol2_02](./CaptainTsubasaVol2_02.png)

![CaptainTsubasaVol2_01](./CaptainTsubasaVol2_01.png)

![CaptainTsubasaVol2_Cup2_HighSchoolSoccer_ToGame6Final_Toho](./CaptainTsubasaVol2_Cup2_HighSchoolSoccer_ToGame6Final_Toho.png)
*Captain Tsubasa Vol2 - Cup 2 High School Soccer - Final Toho*

![CaptainTsubasaVol2_Cup2_HighSchoolSoccer_ToGame5_Furanu](./CaptainTsubasaVol2_Cup2_HighSchoolSoccer_ToGame5_Furanu.png)
*Captain Tsubasa Vol2 - Cup 2 High School Soccer - Game 5 Furanu*

![CaptainTsubasaVol2_Cup2_HighSchoolSoccer_ToGame4_Musashi](./CaptainTsubasaVol2_Cup2_HighSchoolSoccer_ToGame4_Musashi.png)
*Captain Tsubasa Vol2 - Cup 2 High School Soccer - Game 4 Musashi*

![CaptainTsubasaVol2_Cup2_HighSchoolSoccer_ToGame3_Tatsun](./CaptainTsubasaVol2_Cup2_HighSchoolSoccer_ToGame3_Tatsun.png)
*Captain Tsubasa Vol2 - Cup 2 High School Soccer - Game 3 Tatsun*

![CaptainTsubasaVol2_Cup2_HighSchoolSoccer_ToGame2_Akita](./CaptainTsubasaVol2_Cup2_HighSchoolSoccer_ToGame2_Akita.png)
*Captain Tsubasa Vol2 - Cup 2 High School Soccer - Game 2 Akita*

![CaptainTsubasaVol2_Start](./CaptainTsubasaVol2_Start.png)


## <span id="FelixTheCat">[Felix, the cat](#FelixTheCat)</span>

![FelixTheCat_TheEnd](./FelixTheCat_TheEnd.png)

![FelixTheCat_15](./FelixTheCat_15.png)

![FelixTheCat_14](./FelixTheCat_14.png)

![FelixTheCat_13](./FelixTheCat_13.png)

![FelixTheCat_12](./FelixTheCat_12.png)

![FelixTheCat_11](./FelixTheCat_11.png)

![FelixTheCat_Round_9-3](./FelixTheCat_Round_9-3.png)

![FelixTheCat_Round_9-2](./FelixTheCat_Round_9-2.png)

![FelixTheCat_10](./FelixTheCat_10.png)

![FelixTheCat_09](./FelixTheCat_09.png)

![FelixTheCat_Round_9-1](./FelixTheCat_Round_9-1.png)

![FelixTheCat_08](./FelixTheCat_08.png)

![FelixTheCat_07](./FelixTheCat_07.png)

![FelixTheCat_Round_3-3](./FelixTheCat_Round_3-3.png)

![FelixTheCat_Round_3-2](./FelixTheCat_Round_3-2.png)

![FelixTheCat_06](./FelixTheCat_06.png)

![FelixTheCat_05](./FelixTheCat_05.png)

![FelixTheCat_04](./FelixTheCat_04.png)

![FelixTheCat_03](./FelixTheCat_03.png)

![FelixTheCat_02](./FelixTheCat_02.png)

![FelixTheCat_Round_2-3](./FelixTheCat_Round_2-3.png)

![FelixTheCat_01](./FelixTheCat_01.png)

![FelixTheCat_Start](./FelixTheCat_Start.png)


## <span id="Contra">[Contra](#Contra)</span>

![Contra_08](./Contra_08.png)

![Contra_07](./Contra_07.png)

![Contra_06](./Contra_06.png)

![Contra_05](./Contra_05.png)

![Contra_04](./Contra_04.png)

![Contra_03](./Contra_03.png)

![Contra_02](./Contra_02.png)

![Contra_01](./Contra_01.png)

![Contra_Start](./Contra_Start.png)


## <span id="SuperC">[Super C](#SuperC)</span>

![SuperC_08](./SuperC_08.png)

![SuperC_07](./SuperC_07.png)

![SuperC_06](./SuperC_06.png)

![SuperC_05](./SuperC_05.png)

![SuperC_04](./SuperC_04.png)

![SuperC_03](./SuperC_03.png)

![SuperC_02](./SuperC_02.png)

![SuperC_01](./SuperC_01.png)

![SuperC_Start](./SuperC_Start.png)


## <span id="references">[REFERENCES](#references)</span>
* [https://en.wikipedia.org/wiki/Nintendo_Entertainment_System][1]


[1]: https://en.wikipedia.org/wiki/Nintendo_Entertainment_System
