---
title: Fundación - Isaac Asimov
date: "2024-03-25T16:14:37.121Z"
description: "La saga Fundación del autor ruso/americano Isaac Asimov."
tags: libro literatura Fundación IsaacAsimov Asimov
---


![IsaacAsimov_SagaFundacion_Frente](./IsaacAsimov_SagaFundacion_Frente.jpg)
![IsaacAsimov_SagaFundacion_Respaldo](./IsaacAsimov_SagaFundacion_Respaldo.jpg)


## <span id="Fundacion">[Fundación](#Fundacion)</span>
![IsaacAsimov_1-Fundacion_Portada](./IsaacAsimov_1-Fundacion_Portada.jpg)
![IsaacAsimov_1-Fundacion_Contraportada](./IsaacAsimov_1-Fundacion_Contraportada.jpg)

### El ciclo de Trántor, por Carlo Frabetti
Introducción a la trilogía (ciclo de Trántor); de los premios, argumento y estructura.


### Parte I, LOS PSICOHISTORIADORES

#### 1
Se habla de Gaal Dornick, de su viaje a Trántor, la expectativa; se habla de las técnicas para el viaje.

#### 2
Del arrivo de Gaal Dornick a Trántor en la nave, de los protocolos/diligencias de llegada, del viaje en taxi al hotel Luxor; se describe un poco la estación abovedada.

#### 3
Gaal visita la torre de observación y ve el planeta (Trántor), cuando vuelve a su habitación del hotel Luxor lo espera Hari Seldon, se presenta.

#### 4
Primer encuentro entre Gaal y Hari Seldon, usando matemáticas aplican psicohistoria básica para mostrar a grosso modo el estado del imperio.

#### 5
Gaal es detenido e interrogado, exige sus derechos, al día siguiente recibe la visita del abogado Lors Avakim enviado por Hari Seldon; básicamente le explica que esto estaba predicho y lo tranquiliza; la conversación es interrumpida por un guardia, el abogado se retira.

#### 6
En el proceso contra Hari Seldon y Gaal Dornick, guiado por la Comisión de Seguridad Pública encabezada por el presidente Linge Chen, Seldon es interrogado en el estado acerca de las personas que dirige, de sus predicciones, de la caída (de Trántor y del Imperio), del propósito, del proyecto y la Enciclopedia Galáctica; termina el interrogatorio y Seldon se sienta en compañía de Gaal Dornick.

#### 7
El presidente de la Comisión de Seguridad Pública, Linge Chen, verdadero emperador, junto con el resto de la Comisión continúan más privadamente la conversación con H. Seldon y G. Dornick; de las razones para acatar las ideas de Seldon, de la elección entre la muerte y el exilio; H. Seldon acepta el exilio a Términus para llevar a cabo su proyecto con su gente.

#### 8
Después del acuerdo de exilio a Términus para el Proyecto Seldon, H. Seldon y G. Dornick viajan en taxi hacia la Universidad, al llegar un capitán les informa el estado de ley marcial que les aplica a los involucrados, y el plazo de seis meses para preparar el viaje a Términus; en el despacho de H. Seldon hablan de que los seis meses serán suficientes ya que se han estado preparando por dos años y medio como se evidencia en el microfilme que observan; se mencionan los dos "refugios científicos", de la inminente muerte de H. Seldon, de lo que sucederá después de esto; de los sucesores y los acontecimietnos que se irán desarrollando posteriormente.


### Parte II, LOS ENCICLOPEDISTAS

#### 1
El representante del emperador en Términus y presidente de la junta de síndicos del Comité de la Enciclopedia Lewis Pirenne espera una cita con el alcalde de la Ciudad de Términus Salvor Hardin. Hablan de la noticia de la *asumpción* del título de rey por parte del gobernador real de la Prefectura de Anacreonte y la situación para Términus que esto conlleva (incomunicación, importación de metal, ...), de los roles que le corresponde a cada actor (Lewis Pirenne, Salvor Hardin, el nuevo rey de Anacreonte, el Imperio, ...), del periódico de Ciudad Términus (El Diario de Ciudad Términus) y de la próxima visita del enviado especial Anacreonte.

#### 2
Entrevista entre Anselm ilustre Rodric, Salvor Hardin (alcalde de la Ciudad de Términus) y Lewis Pirenne (presidente de la junta de síndicos del Comité de la Enciclopedia). Visitan la Plaza de la Enciclopedia y luego hablan de la situación de la periferia, del ofrecimienbto de Anacreonte (protección) y del nivel científico de los planetas de la periferia.

#### 3
Al alcalde Salvor Hardin se le permite estar como representante del periódico El Diario en la discusión de la Junta de Síndicos. Hablan de la visita del canciller del Emperador, de la impotencia, de la función de la Enciclopedia, de tomar medidas con respecto a la situación de la periferia, de la esperanza en la Bóveda de Hari Seldon con motivo del 50º aniversario de la Fundación.

#### 4
El canciller del Imperio Lord Dorwin visita Términus y se reúne con Lewis Pirenne y Salvor Hardin. Se menciona la situación con Anacreonte, hablan de arqueología, de la explosión de la planta atómica en el Planeta V de Gamma Andromeda y del estado actual de los sistemas de la periferia.

#### 5
En la segunda reunión de la Junta de Síndicos a la que el alcalde Salvor Hardin asiste se analiza el tratado entre Anacreonte y el Imperio, y la visita del canciller, de las acciones a tomar para afrontar la situación con Anacreonte; Salvor Hardin hace una reprimenda a la Junta por su actitud tan pasiva, se habla del análisis que Hari Seldon pudo haber hecho de la situación actual.

#### 6
Salvor Hardin acompañado por Yohan Lee dialogan del golpe de estado próximo a efectuarse debido a la incompetencia de la Junta para gobernar a Términus, al final se cuestiona sus próximas acciones.

#### 7
El mensaje de la Bóveda de Seldon explicando la verdadera naturaleza de la Fundación y del curso de acontecimientos seguidos hasta ahora, junto con el anuncio de la 1ª Crisis Seldon es presentado.


### Parte III, LOS ALCALDES

#### 1
De la reunión entre Salvor Hardin (alcalde de Términus), acompañado por Yohan Lee, y Sef Sermak (concejal de Términus), acompañado por otros tres. Sermak hace un últimatum a Hardin con la única opción de que dimita, él y su partido (activista) recién formado no están de acuerdo con las políticas exteriores del gobierno; se hace referencia a la proximidad de las elecciones, también Yohan Lee había hecho referencia a los seguidores de Sermak y a tener cuidado con él y no subestimarlo. En la entrevista Hardin explica la situación en la que se convirtió en alcalde y la justificación de porqué ha llevado la política que ha llevado, Sermak refuta con algunos recelos; la reunión concluye cuando Hardin recibe un cilindro con un mensaje y dice que el gobierno sabe lo que hace. Al final Hardin y Lee intercambian impresiones, Lee aboga por cautela, de la orden de vigilar a Sermak y sus hombres; el mensaje recibido decía que el embajador Verisof vuelve a Términus, se especula acerca de posibles complicaciones.

#### 2
De la entrevista entre Salvor Hardin y Poly Verisof (embajador sacerdote en Anacreonte). Hablan de la amenaza que es Wienis (príncipe regente y tío del rey Leopold), del hallazgo por parte de una nave mercante anacreontiana de un crucero de batalla imperial, de la delicada situación entre Anacreonte Y Términus, de la postura de Hardin de mantener un equilibrio de poderes el mayor tiempo posible hasta que sólo quede una línea de acción para no desviarse del Plan Seldon.

#### 3
Cuando Leoplod I, rey de Anacreonte (Señor de los Dominios exteriores) se prepara para la tradicional caza del ave Nyak es interpelado por su tío Wienis, príncipe regente. En la habitación de Wienis, éste trata de persuadir a Leopold hacia la idea de guerra contra la Fundación sin importar cautela/creencias/fé; al final Leopold reflexiona suspicazmente hacia Wienis.

#### 4
Sef Sermak (concejal de Términus) y los miembros de su partido (activista) tienen una reunión donde el enviado a Anacreonte Lewis Bort reporta lo observado, y se discute la dificultad de influir en Anacreonte instigando una revuelta debido a la caracterización divina del monarca establecida indirectamente por Hardin y su política, de la situación y acciones del partido.

#### 5
En una mañana, el día siguiente después de una agitada sesión del Concejo Municipal donde la moción de procesar a Salvor Hardin por alta traición propuesta por Sef Sermak fue derrotada, Salvor Hardin se escabulle hacia una nave con rumbo a Anacreonte, como lo tenía planeado; en el camino a la nave lo acompaña Yohan Lee, dialogan, hablan de la Bóveda Seldon y Hardin da algunas instrucciones a Lee.

#### 6
Hardin da un corto recorrido por los ocho mayores sistemas estelares del reino de Anacreonte entrevistándose con los representantes locales de la Fundación para al final dirigirse al planeta Anacreonte justo para presenciar la ceremonia/festividad de coronación del rey Leopold; logra una entrevista corta con el embajador y máximo sacerdote en Anacreonte, Poly Verisof, para después participar retiradamente de la ceremonia de coronación; justo antes de la culminación del rito el príncipe regente, tío de Leopold, Wienis contacta con Hardin y lo invita a hablar en sus aposentos privados. Compartiendo una copa de vino empiezan a hablar de los alcances de Anacreonte, del papel de la Fundación; Wienis informa a Hardin que en ese momento la guerra está a punto de estallar con el ataque a la Fundación por parte de Anacreonte, Hardin es declarado prisionero de guerra, Hardin anuncia que tiene planeado un contraataque consistente en una huelga de los sacerdotes; cuando Wienis vuelve a la sala de la ceremonia, en el punto de culminación del acto, algo falla y la aureola radiactiva del rey se apaga, Wienis trata de calmar/controlar la situación, se lleva a Leopold a sus aposentos, Hardin saluda al rey, brinda explicaciones de los efectos de su contraataque en Anacreonte, Wienis trata de apaciguar/rebatir estos argumentos, y al final Hardin pretende mostrarles una sorpresa en el TV que tiene que ver con un relevador de ultraondas.

#### 7
Theo Aporat, sacerdote de alta categoría, directamente vinculado al crucero imperial y su restauración maldice la nave, la inhabilita, toma el control, arresta al almirante Lefkin, hijo de Wienis (príncipe regente), ordena que se comunique con el resto de la flota y con Anacreonte.

#### 8
En los aposentos de Wienis en Anacreonte, vía TV son testigos de la toma del crucero imperial y de la declaración de Lefkin, se aprecia la aureola (campo de fuerza) de Hardin, cuenta una fábula, Wienis intenta dispararle a Hardin, no logra su objetivo, al final se suicida.

#### 9
Se menciona el retorno triunfal y popularidad aumentada de Hardin después de la derrota total de Anacreonte, de las disculpas de Sermak y sus compañeros, Lee insiste en sus preocupaciones; una gran congregación atiende el 2º mensaje en la Bóveda Seldon. El mensaje habla de la superación de la 2ª crisis, advierte que el poder espiritual no será suficiente, menciona la otra Fundación.


### Parte IV, LOS COMERCIANTES

#### 1
Limmar Ponyets recibe un mensaje y visitante, Gorm le entrega un mensaje informándole que Eskel Gorov, realmente un agente de la Fundación, había sido capturado en Askone.

#### 2
Limmar Ponyets va hasta Askone, logra entrevistarse con el gran maestre, intentan negociar algo; al final hablan de creencias.

#### 3
Limmar Ponyets se entrevista con Eskel Gorov en su celda, Ponyets se entera de que el interés del gran maestre es oro, se habla de que el objetivo es aumentar la seguridad de la Fundación en la Periferia formando un imperio comercial controlado por la religión; la idea es hacer pequeñas ventas estratégicas a nobles para crear una facción pro-atómica en la Corte, Ponyets muestra interés en el aspecto comercial del asunto.

#### 4
Ponyets hace su demostración del transmutador para convertir hierro en oro, tiene éxito, les habla persuasivamente, les ofrece la garantía de un tiempo de observación; la aceptación es unánime.

#### 5
Ponyets y Phrel (consejero del gran maestre) hacen negocios.

#### 6
Gorov (ya liberado) y Ponyets son escoltados por la flota particular de Phrel a sus propiedades mineras en las afueras de Askone para cargar tanto estaño como puedan transportar como pago por toda la mercancía que tenían Gorov y Ponyets después de que Ponyets logró chantajear a Phrel y hacer ese trato; Gorov y Ponyets hablan de esto por el rayo antidistortivo.


### Parte V, LOS PRÍNCIPES COMERCIANTES

#### 1
El secretario del alcalde Jorane Sutt habla con el maestro comerciante Hober Mallow del planeta Smyrno (uno de los antiguos Cuatro Reinos del tiempo de la Convención de la Fundación) acerca de la pérdida de tres naves comerciantes en el último año en el territorio de la República de Korell, y de la posibilidad de que Korell posea armas atómicas. Sutt pide a Mallow que acuda a Korell en calidad de comerciante, pero con los ojos abiertos.

#### 2
En su apartamento de soltero en el Edificio Hardin, Jorane Sutt (secretario del alcalde) y Publis Manlio (secretario del Exterior del gabinete del alcalde, y para el resto de sistemas: primado de la Iglesia, suministrador del Alimento Sagrado, maestro de los templos, etc.) hablan de la tarea encomendada a Hober Mallow, de la posibilidad de que ésta sea una Crisis Seldon, y de cuáles serían los planes para enfrentarla.

#### 3
Hober Mallow y Jaim Twer, un veterano comerciante, hablan de la propuesta de Twer a Mallow para que sea el candidato/representante de la campaña de Twer para tener una representación en el Consejo, Mallow le informa que está ocupado con negocios pendientes, hablan de las crisis Seldon, finalmente Mallow invita a Twer a ser parte de su tripulación.

#### 4
Se describe brevemente Korell y su puerto espacial. Después de siete días de espera en el Estrella Lejana se recibe a Jord Parma, un supuesto misionero de la Fundación, quien necesita atención, la nave está en estado de alerta, la presencia del misionero en esa región es ilegal, Mallow no puede y no quiere protegerlo, cuando los korelianos lo reclaman, Mallow lo entrega; al final, media hora después, reciben un comunicado invitándolos a comparecer ante el augusto comodoro de Korell, Asper Argo.

#### 5
Paseando por el jardín de la "casa" del comodoro Asper Argo, éste y Mallow hablan; Mallow comenta acerca de la relación de sus naciones y el comercio entre ellas, el comodoro sugiere reticencia, pero Mallow dice que sólo el comercio y el dinero son su intrés, nada que tenga que ver con la religión está en sus propuestas; una de las sirvientas de la comodora sirve de modelo para la demostración del cinturón ornamental que presenta Mallow, explica los términos básicos de los negocios y comercialización, al final hablan de la demostración de un artículo para las fundiciones de acero.

#### 6
El Augusto Comodoro de la República de Korell, Argo Asper, dialoga con su esposa, la comodora Licia; hablan de la cena programada con Mallow y sus hombres, se menciona al padre de Licia que parece ser poderoso, el comodoro entrega el cinturón a su esposa, no hablan más.

#### 7
Jaim Twer, el veterano comerciante invitado a el Estrella Lejana por Mallow y líder del movimiento comerciante habla con Mallow, quien cavila de la facilidad para acceder a las fundiciones de Korell, y la posibilidad de que tengan o vayan a hallar armas atómicas o indicios de su existencia, se comenta que Twer no estuvo en la fiesta de ayer por los términos de la entrevista fijados por el comodoro; un automóvil espera para llevarlos a la fundición.

#### 8
En la fundición, atento a la visita del comodoro y su corte, Mallow hace las demostraciones de la sierra atómica; en un momento aprecia que las armas portátiles de los guardaespaldas son atómicas y tienen las insignias del antiguo imperio galáctico.

#### 9
En su camarote personal de la Estrella Lejana en el espacio, Hober Mallow da instrucciones al teniente Drawt ya que se dispone a partir.

#### 10
Hober Mallow visita a Onum Barr (viejo que fue un antiguo patricio del imperio) en Siwenna, el viejo le habla de la historia de Siwenna y de su propia historia y familia, Mallow también averigua acerca de la energía atómica que posee Siwenna, Onum Barr le proporciona información y ayuda a Mallow para llegar a la estación de energía más cercana.

#### 11
En la visita a la estación de energía acompañado por el técnico, Mallow sugiere un interés particular al técnico, el técnico finge ofenderse y repudiar el soborno en primera instancia, Mallow lo persuade con tres argumentos convincentes, terminan haciendo negocios.

#### 12
En un fugaz paseo por las instalaciones de la estación de energía, Mallow ve los generadores sin tocar nada; en el despacho del técnico Mallow intenta obtener más información sin éxito; retorna al Estrella Lejana.

#### 13
Hober Mallow descansa en su nueva casa, está acompañado por Ankor Jael, un diplomático expulsado del gabinete ex-ministro de Educación, hablan de la incursión en la política/concejo de Mallow mientras esperan la visita del secretario del alcalde Jorane Sutt; en el momento del arrivo de éste concuerdan en que Joel se esconda en la habitación contigua para que escuche la conversación. Mallow y Sutt hablan acerca de la misión en Korell, del informe, del orígen de la reciente fortuna de Mallow, de los términos de este comercio establecido por Mallow, de las implicaciones/incidencias con la filosofía/política/costumbres de la Fundación, Sutt trata de comprar a Mallow, Sutt amenaza con un proceso por el asesinato del misionero de la Fundación Jord Parma (entregado a una muchedumbre para linchamiento). Cuando termina la visita, Mallow y Jael dialogan, Mallow pide a Jael que le comparta sus impresiones, de los posibles objetivos/intenciones/planes de Sutt, y del problema serio que representa la amenaza de proceso por asesinato por parte de Sutt para los planes de Mallow.

#### 14
En el concurrido cuarto día del juicio público de Hober Mallow éste presenta su defensa cuyo núcleo es una grabación en la que se aprecia que el supuesto misionero de la Fundación Jord Parma era un miembro de la PSK (Policía Secreta Korelliana); al terminar Mallow es vitoreado.

#### 15
Unos días después Ankor Jael y Hober Mallow dialogan, hablan acerca de quitar a sus rivales del camino, de los planes políticos de Mallow, de la proximidad de ésta Crisis Seldon, y de los planes de Mallow para superarla, hacer nada.

#### 16
Comodoro y comodora hablan, la comodora le dice que está enterada de las decisiones de él; habrá guerra entre Korell y la Fundación.

#### 17
En el espacio la nave Nebulosa Oscura detecta la presencia de una amenazante nave korelliana que antiguamente perteneció al imperio galáctico; se preparan y envían un mensaje a la Fundación.

#### 18
Mallow, como alcalde, revisa informes de la guerra, habla con Ankor Jael, nombrado ministro de Educación y Propaganda, esperan a Jorane Sutt, cuando llega Mallow y Sutt hablan en presencia de Jael. Pretendiendo una alianza/acuerdo con Sutt, Mallow escucha su posición, Mallow lo contra-argumenta y le sugiere que le explicará su política amenazándolo con las opciones de estar de acuerdo y obtener un puesto en el gabinete o ser un mártir en la cárcel; Mallow expone su visión, enfrentar la situación usando el subestimado comercio, el simple solo comercio causando necesidades básicas e individuales en las bases de la sociedad (gente del común), subiendo jerarquías hasta los financieros, industriales, etc., y contando con la imposibilidad por parte del imperio de suplir éstas necesidades de pequeña escala, causando en general una desaveniencia del pueblo hacia el gobernante; Sutt no confía en él y lo supone un traidor a favor del imperio, de este modo no hay acuerdo y Mallow no duda en arrestarlo; al final, hablando con Jael, Mallow amplía su visión del doble filo del comercio, Jael deduce que terminará por establecerse una plutocracia, Mallow reflexiona que el futuro se resolverá en su tiempo. Años después de apaciguada guerra, Korell se rinde y Hober Mallow también pasa a la historia.


## <span id="FundacionImperio">[Fundación e Imperio](#FundacionImperio)</span>
![IsaacAsimov_2-FundacionImperio_Portada](./IsaacAsimov_2-FundacionImperio_Portada.jpg)
![IsaacAsimov_2-FundacionImperio_Contraportada](./IsaacAsimov_2-FundacionImperio_Contraportada.jpg)


## <span id="SegundaFundacion">[Segunda Fundación](#SegundaFundacion)</span>
![IsaacAsimov_3-SegundaFundacion_Portada](./IsaacAsimov_3-SegundaFundacion_Portada.jpg)
![IsaacAsimov_3-SegundaFundacion_Contraportada](./IsaacAsimov_3-SegundaFundacion_Contraportada.jpg)


## <span id="referencias">[REFERENCIAS](#referencias)</span>
* [https://es.wikipedia.org/wiki/Fundaci%C3%B3n_(novela)][1]
* [https://es.wikipedia.org/wiki/Isaac_Asimov][2]


[1]: https://es.wikipedia.org/wiki/Fundaci%C3%B3n_(novela)
[2]: https://es.wikipedia.org/wiki/Isaac_Asimov
