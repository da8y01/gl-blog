---
title: Online platforms exercises
date: "2024-03-28T18:52:50.121Z"
description: "Programming exercises from on-line platforms tests."
tags: ProgrammingExercises programming exercises TuringCom code
image: ./TuringCom.png
---


## <span id="WealthiestOwner">[Wealthiest owner](#WealthiestOwner)</span>

![TuringCom_WealthiestOwner1_2024-01-10](./TuringCom_WealthiestOwner1_2024-01-10.png)
![TuringCom_WealthiestOwner2_2024-01-10](./TuringCom_WealthiestOwner2_2024-01-10.png)

You are given an 2D matrix of *lockers* where *lockers[m][n]* represent the value of locker and *xxx* represent owner with *xxx* as locker.<br />
Provide the **net worth** of the wealthiest owner, *calculate as the sum total of the value they possess in all of their* lockers.

Example 1:<br />
Input: lockers = [[5,6,7],[3,4,3]]<br />
Output: 18<br />
Explanation:<br />
1st owner has wealth = 5 + 6 + 7 = 18<br />
2nd owner has wealth = 3 + 4 + 3 = 10<br />
The 1st owner is considered the wealthiest with a wealth of 18, so return 18.

Example 2:<br />
Input: lockers = [[1,5],[7,3],[3,5]]<br />
Output: 10<br />
Explanation:<br />
1st owner has wealth = 6<br />
2nd owner has wealth = 10<br />
3rd owner has wealth = 8<br />
The 2nd owner is the richest with a wealth of 10.

Constraints:
* m == lockers.length
* n == lockers[i].length
* 1 <= m, n <= 50
* 1 <= lockers[i][j] <= 100

### [Python][3] ([WealthiestOwner1.py](./WealthiestOwner1_py.txt))
```python{numberLines: true}
def solution(k) -> int:
  res = -1
  # write your solution here
  print(k)
  accum = 0
  wealths = []
  for idx1, elm1 in enumerate(k):
    for idx2, elm2 in enumerate(k[idx1]):
      accum = accum + k[idx1][idx2]
    wealths.append(accum)
    accum = 0
  print('wealths', wealths)
  res = max(wealths)
  return res


# R E A D M E
# DO NOT CHANGE the code below, we use it to grade your submission. If...
if __name__ == "__main__":
  # mtx = [[int(val) for val in pair.split()] for pair in input().strip()]
  mtx = [[5,6,7],[3,4,3]] # result 18
  # mtx = [[1,5],[7,3],[3,5]]  # result 10

  print(solution(mtx))
```

![WealthiestOwner1_TerminalPython](./WealthiestOwner1_TerminalPython.png)


## <span id="RearrangeString">[Rearrange The String](#RearrangeString)</span>

![TuringCom_RearrangeString1_2024-01-10](./TuringCom_RearrangeString1_2024-01-10.png)
![TuringCom_RearrangeString2_2024-01-10](./TuringCom_RearrangeString2_2024-01-10.png)

You are given a string *str* consisting of letters and numbers.<br />
You need to find a combination of the string where one letter is not adjacent to another letter and filled by number.<br />
Return **the string** in ascending order with above combinations or return blank string if it is not possible to create the result.

Rules:
1. If letters and numbers are equally distributed, number will get first preference.
2. Higher character count will get preference

**Example 1:**<br />
**Input:** str = "z3b1a2"<br />
**Output:** "1a2b3z"<br />
**Explanation:** No two adjacent characters have the same type in "1a2b3z".

**Example 2:**<br />
**Input:** str = "q56"<br />
**Output:** "5q6"<br />
**Explanation:** No two adjacent characters have the same type in "5q6".

**Constraints:**
* 1 <= str.length <= 500
* str consists of only letters and/or numbers.

### [Python][3] ([RearrangeString1.py](./RearrangeString1_py.txt))
```python{numberLines: true}
from typing import List

def walk_build(str_build, iteratable):
  for idx_tuple, itm_tuple in enumerate(iteratable):
      for item in iteratable[idx_tuple]:
        str_build = str_build + item
  return str_build

def solution(s: str) -> str:
  # write your solution here
  print(s)
  numbers = []
  letters = []
  arranged = ''

  for char in s:
    if char.isnumeric():
      numbers.append(char)
    else:
      letters.append(char)

  numbers = sorted(numbers)
  letters = sorted(letters)

  if abs(len(numbers) - len(letters)) > 1:
    return ''
  
  if len(numbers) - len(letters) == 0:
    zipped = list(zip(numbers, letters))
    return walk_build(arranged, zipped)
  
  if abs(len(numbers) - len(letters)) == 1:
    if len(numbers) > len(letters):
      arranged = arranged + numbers[0]
      zipped = list(zip(letters, numbers[1:]))
    if len(letters) > len(numbers):
      arranged = arranged + letters[0]
      zipped = list(zip(numbers, letters[1:]))

    return walk_build(arranged, zipped)

# R E A D M E
# DO NOT CHANGE the code below, we use it to grade your submission. If...
if __name__ == '__main__':
  # a = input()
  # a = "q566"
  a = "z3b1a2"
  # a = "9gda7e86"
  # a = "q56"
  # a = "qw5"
  output = solution(a)
  print(output)
```

![RearrangeString1_TerminalPython](./RearrangeString1_TerminalPython.png)


## <span id="CommonNumbers">[Common Numbers](#CommonNumbers)</span>

![TuringCom_CommonNumbers1_2024-01-16](./TuringCom_CommonNumbers1_2024-01-16.png)
![TuringCom_CommonNumbers2_2024-01-16](./TuringCom_CommonNumbers2_2024-01-16.png)

Create a program that takes in two sets of integers, "set1" and "set2", and returns a new set that contains only the integers that are present in both sets. The no. of times an integer is present in both the sets should be the no. of times it is present in the resultant set as well. The order of the integers in the result set should be in ascending order.

**Example 1:**<br />
**Input:** set1 = [12,11,11,12], set2 = [12,12]<br />
**Output:** [12,12]<br />

**Example 2:**<br />
**Input:** set1 = [14,19,15,14], set2 = [19,14,19,18,14]<br />
**Output:** [14,19,14]<br />
**Explanation:** [19,14,14] or [14,14,19] is also accepted.

**Constraints:**
* 1 <= nums1.length, nums2.length <= 1000
* 0 <= nums1[i], nums2[i] <= 1000

### [JavaScript][5] ([CommonNumbers1.js](./CommonNumbers1_js.txt))
```javascript{numberLines: true}
var solution = function(x, y) {
    // write your solution here
    let res = [];
    return res;
};

/**
R E A D M E
DO NOT CHANGE the code below, we use it to grade your submission. If...
**/
function printArray(arr) {
    console.log(JSON.stringify(arr).replace(/,\s*/g, ','));
}

const a = readline().split(" ").map(n => parseInt(n));
const b = readline().split(" ").map(n => parseInt(n));
const output = solution(a, b);
printArray(output);
```

![CommonNumbers1_TerminalJS](./CommonNumbers1_TerminalJS.png)


## <span id="BinaryTree">[Binary Tree](#BinaryTree)</span>

![TuringCom_BinaryTree1_2024-01-16](./TuringCom_BinaryTree1_2024-01-16.png)
![TuringCom_BinaryTree2_2024-01-16](./TuringCom_BinaryTree2_2024-01-16.png)
![TuringCom_BinaryTree3_2024-01-16](./TuringCom_BinaryTree3_2024-01-16.png)
![TuringCom_BinaryTree4_2024-01-16](./TuringCom_BinaryTree4_2024-01-16.png)
![TuringCom_BinaryTree5_2024-01-16](./TuringCom_BinaryTree5_2024-01-16.png)
![TuringCom_BinaryTree6_2024-01-16](./TuringCom_BinaryTree6_2024-01-16.png)

Find the sum of all left leaves in a given binary tree.

**Example:**<br />
3
/\
9 20
  /\
  15 7

There are two left leaves in the binary tree, with values 9 and 15 respectively. Return 24.

### [JavaScript][5] ([BinaryTree1.js](./BinaryTree1_js.txt))
```javascript{numberLines: true}
/**
 * @param TreeNode root
 * @return int
 */
function sumLeftLeaves(root) {
  return 0
}

// /**
// R E A D ME
// DO NOT CHANGE the code below, we use it to grade your submission. If...
// **/

function TreeNode(data) {}
function findNode(node, value) {}

function deserialize(queue) {}

function buildQueue(raw) {}

// // read inputs
const input = readline();
const queue = buildQueue(input.split(","));
const tree = queue[0] !== null ? deserialize(queue) : null;

// // solution
const output = sumLeftLeaves(tree);
// print the output
console.log(output);
```

![BinaryTree1_TerminalJS](./BinaryTree1_TerminalJS.png)


## <span id="references">[REFERENCES](#references)</span>
* [https://turing.com][1]
* [https://developers.turing.com][2]
* [https://www.python.org][3]
* [https://nodejs.org/en][4]
* [https://www.javascript.com/][5]


[1]: https://turing.com
[2]: https://developers.turing.com
[3]: https://www.python.org
[4]: https://nodejs.org/en
[5]: https://www.javascript.com/
