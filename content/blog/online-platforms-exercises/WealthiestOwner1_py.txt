def solution(k) -> int:
  res = -1
  # write your solution here
  print(k)
  accum = 0
  wealths = []
  for idx1, elm1 in enumerate(k):
    for idx2, elm2 in enumerate(k[idx1]):
      accum = accum + k[idx1][idx2]
    wealths.append(accum)
    accum = 0
  print('wealths', wealths)
  res = max(wealths)
  return res


# R E A D M E
# DO NOT CHANGE the code below, we use it to grade your submission. If...
if __name__ == "__main__":
  # mtx = [[int(val) for val in pair.split()] for pair in input().strip()]
  mtx = [[5,6,7],[3,4,3]] # result 18
  # mtx = [[1,5],[7,3],[3,5]]  # result 10

  print(solution(mtx))