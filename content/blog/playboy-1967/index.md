---
title: Playboy 1967
date: "2023-10-16T13:33:37.121Z"
description: "A real Playboy magazine from january 1967."
tags: playboy 1967 magazine
---


![Playboy1967_Cover](./Playboy1967_Cover.jpg)

![Playboy1967_Back](./Playboy1967_Back.jpg)


## <span id="references">[REFERENCES](#references)</span>
* [https://en.wikipedia.org/wiki/Playboy][1]


[1]: https://en.wikipedia.org/wiki/Playboy
