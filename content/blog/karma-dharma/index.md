---
title: Karma & Dharma - Annie Besant
date: "2024-05-13T21:57:02.121Z"
description: "Los libros 'Karma' y 'Dharma' de la autora inglesa Annie Besant."
tags: libro inglaterra Karma Dharma KarmaDharma AnnieBesant
---


![KarmaDharma_AnnieBesant_Cover](./image-placeholder-icon-7.png)


## <span id="karma01">[Karma](#karma01)</span>
Se describe como el pensamiento cambia/evoluciona/transforma internamente conviertiéndose en una entidad activa en su Mundo, y cómo esta corriente y reacciones a interacciones son llamadas *Spandha* o *Karma*.


## <span id="karma02">[La estabilidad de la ley](#karma02)</span>
Se describe la Ley que sostiene todo. 


## <span id="karma03">[Los tres planos de la Naturaleza](#karma03)</span>
Se acuñan los tres planos de la naturaleza, de las relaciones tipo Materia-cuerpo, Espíritu-vida, la sensación como la propiedad característica más saliente de los Elementos Kámicos.


## <span id="karma04">[Producción de formas de pensamiento](#karma04)</span>
La mente produce imágenes, la imaginación es la facultad creadora de la mente, las palabras tratan de describir las imágenes y hechos salientes de las ideas, los conflictos surgen por asociación errónea entre imágenes y palabras, las formas de pensamiento o imagen mental producen vibraciones en derredor que hacen una transición a materia más densa de regiones psíquicas inferiores que atraen al Elemental correspondiente, se menciona que los Elementales pertenecen a uno de los Siete Rayos Primarios, los Siete Hijos primordiales de la Luz, los Siete Espíritus que están delante del Trono, la comunicación con los Elementales es por medio de palabras-colores, se forman entidades astrales acordes a la forma de pensamiento constituyendo una hueste Kármica.


## <span id="karma05">[Actividad de las formas de pensamiento](#karma05)</span>
El período de vida de estas formas de pensamiento depende de la intensidad inicial y del alimento que se les suministra, la relación con sus progenitores se denomina "lazo magnético", el pequeño ciclo Kármico de que la mente la forja, la lleva y, al llevarla, puede limarla, las formas de pensamiento pueden ser dirigidas al exterior o atraídas del exterior, las agregaciones de formas de pensamiento similares y la atmósfera local.


## <span id="karma06">[Formación del Karma en conjunto](#karma06)</span>
Del concepto "período de vida", los cuatro diferentes estados por los que pasa el Alma (experiencias físicas, astrales y devachánicas, regreso a plana físico), la jornada del Eterno Peregrino, la labor de la ley Kármica también comprende la actividad del Alma en el estado no físico, se diferencia Imagen Mental y forma de pensamiento, sin Imagen Mental no habría Karma, de la Imagen astro-mental (ó *entidad activa*), la actividad del Alma crea el Karma como causa.

Kâma, Deseo.
Akâsha, la materia del Mundo emanada del Logos mismo.

más potente que la desdicha es la voluntad


## <span id="karma07">[Formación del Karma en detalle](#karma07)</span>
Reconocer el Ego, Manas Superior e Inferior, para efectos prácticos el Manas Inferior es el "Yo" y el Manas Superior es el Ego Personal o Dios, pero Manas Inferior y Superior son uno, el Ego que forma el Karma es el que lo recoge, de la formación del Karma por la Imagen Mental, el Ego del Alma se va purificando al transitar hacia planos superiores, las Imágenes Mentales van transmutando, su forma antigua/original sólo puede verse en los Anales Kármicos, en la experiencia devachánica el Alma re-asimila las Imágenes Mentales, el Alma extrae de las Imágenes Mentales por la meditación paciente todas las lecciones que encierran, se explica el nacimiento y desarrollo de la Conciencia, la Ley Kármica operando con Imágenes Astro-mentales se aborda mejor en la extinción del Karma.

ningún pensamiento, por pasajero que sea, deja de estar representado

Kama Loka o Lugar del Deseo

cuerpo de deseos o Kama-Rupa

La envoltura Kamarrûpica se purifica de sus elementos más groseros, al paso que el Ego Inferior es atraído hacia lo alto o hacia adentro, hacia la región Devachánica

Esta misma ley se aplica igualmente a las Imágenes Mentales originadas por deseos groseros, aun cuando éstas jamás pasan al Devachán; sino que están sujetas al procedimiento antes descrito, para ser realizadas a la vuelta a la tierra.

El Karma causativo es completo, y el acto físico se conviete en su efecto inevitable, cuando llega al punto en que una nueva repetición de la Imagen Mental significa que ha pasado a ser un hecho.

Cuando la agregación de las Imágenes Mentales ha llegado al punto de saturación, la adición de una sola más las solidifca en el acto.

... y lo físico es constreñido a obedecer el impulso mental.

De las Imágenes Mentales de las experiencias, y particularmente de las que muestran cómo ha sido causado el sufrimiento por la ignorancia de la ley, nace y se desarrolla la Conciencia.

Las aspiraciones y Deseos se convierten en ... Capacidades.
Los Pensamientos repetidos en ... Tendencias.
La Voluntad de ejecutar en ... Acciones.
Las experiencias en ... Sabiduría.
Las experiencias dolorosas en ... Conciencia.


## <span id="karma08">[Extinción del Karma](#karma08)</span>
La última etapa del período de vida del Alma después de pasar por el vestíbulo del Devachán es el plano de la Reencarnación donde es atraída de nuevo a la tierra por los lazos del Deseo ocurriendo un lento proceso que se va apresurando de Evolución del Alma; los Grandes Señores del Karma otorgan el construído Doble Etéreo, el Hombre se crea a sí mismo; para el nuevo hombre que nace, su campo de acción, instrumentos reguladores y circunstancias son determinadas por las poderosas Inteligencias Espirituales denominadas los "Lípika" y los "Cuatro Mahârâjahs", son los "Segundos Siete" y a su cargo están los Anales Astrales llenos de las Imágenes Akáshicas, el Doble Etéreo es llevado a las condiciones más convenientes para extinguir el llamado Karma Prârabdha, el Alma ve confirmada la Justicia de la Buena Ley llevada por la Sabiduría Divina; los Señores del Karma y el mismo Ego influyen en el destino del individuo, el Ego suministra todos los materiales, el Ego forma el carácter, los Señores del Karma construyen el cuerpo que se adapta y ajusta de modo que la Buena Ley sea expresada.

Manvántara

el Hombre, verdaderamente, se crea a sí mismo; es responsable, en todo el sentido de la palabra, de cuanto es.

Ya hemos visto que los *Pensamientos forman el Carácter*; veamos ahora cómo *las Acciones determinan el medio en que se vive*.

Los medios no aprovechados se transforman en aspiraciones frustradas, en deseos que no encuentran modo de realizarse, en tendencias vehementes de prestar ayuda, contenidas por falta de poder para realizarlas, ya sea por razón de incapacidad, ya sea por ausencia de toda ocasión.

Sin embargo, los designios del Karma son iguales, aunque difíciles de ver, excepto por aquellos cuyos ojos se han abierto.

El auxilio dado se revierte al dador en forma de asistencia espiritual y mental que de derecho le corresponde.


## <span id="karma09">[Hacer frente a los resultados Kármicos](#karma09)</span>
El hombre no es un esclavo del Destino, sino que puede aprovecharse de la Ley para dominar el Destino; se muestra que la Necesidad y el Libre Albedrío obran de consuno y en armonía.

Y, sin embargo, *él*, el Hombre Verdadero, no está cohibido.

El que forjó el pasado que aprisiona su presente, puede obrar dentro de su prisión y crear un porvenir de libertad.

Pero para el hombre vulgar a quien el conocimiento llega en forma de chispa y no de llama, el primer paso hacia la libertad será aceptar sus limitaciones como hechura propia, y proceder a alejarlas.

Esto significa que pecó como pensador en el pasado, y por ello sufre como actor en el presente.

Cometió una manifiesta injusticia, y sufre la pena; la pensó en el pasado, de otro modo no la hubiera sufrido ahora; sufrirá con paciencia el castigo merecido a su pensamiento, y hoy pensará de modo que su mañana se vea libre de vergüenzas.


## <span id="karma10">[Formación del Carácter](#karma10)</span>
La Religión de la Sabiduría enseña que se puede adelantar a la evolución de los mundos; cuando se comprende la Ley Eterna y la manera de obrar del Karma se procede a formar el carácter.

Sabiendo que se convierte en lo que piensa, medita deliberada y regularmente en un noble ideal.

Durante la vida terrestre reúne experiencias, utilizando todo lo que se encuentra a su paso; en el Devachán se las asimila y traza la ruta del porvenir.

Cuando un hombre comprende en parte la manera de obrar del Karma y la acepta, puede principiar desde luego la formación de su carácter, labrando cada faceta con especial cuidado, consciente de que lo hace para la eternidad.

... pues el Alma es arquitecto al mismo tiempo que constructor...


## <span id="karma11">[Cómo se moldea el Karma](#karma11)</span>
Más allá de construir su propio porvenir, se puede construir el destino, se puede paralizar el Karma malo, y moldear el Karma. El Karma puede modificarse y calcular los efectos de energías/fuerzas conjuntas para dar la dirección deseada; analizando las causas pasadas y calculando los efectos futuros se puede lograr el *equilibrio* o liberación.


## <span id="karma12">[Cesasión del Karma](#karma12)</span>
Tanto el Karma bueno como malo forjan una cadena que sujeta con igual fuerza; sólo destruyendo las ligaduras del deseo y el apego que sujetan, el Alma queda libre conviertiéndose en la Vida Libertada, el Yo permanece sereno sin forjar las cadenas del tiempo; cesar el Karma con la "inacción en la acción"; el hombre que ha aprendido el Alma de la Vida Libertada no puede ser detenido por el Karma y no forja cadenas que lo sujeten, y su tarea es tan sólo trabajar para ser útil a los demás; el lazo de la unidad real, el que une al uno con el Todo no se rompe jamás.

**Ejecuta siempre las acciones que constituyan tu deber, pero sin apego niguno, porque obrando el hombre sin interesarse en los resultados, es como llega a lo supremo.**

Yoga: acción

**Pues muertas sus aficiones hallándose en armonía sus pensamientos puestos en la sabiduría, siendo sus obras sacrificios, todas sus acciones se disuelven.**

**Así como el ignorante obra por apego a la acción, ¡oh Bhárata! así el sabio obra sin aficiones, deseando sólo el sostenimiento de la Humanidad.**

... precisamente porque no desea nada para sí, puede darlo todo a los demás.


## <span id="karma13">[El Karma colectivo](#karma13)</span>
La reunión de Almas en grupos forman familias, castas, naciones y razas, suceden los llamados "accidentes" y ajustes; el hombre también es dueño colectivamente de su destino, el mundo ha sido formado por su acción creadora; las Formas de Pensamiento pueden juntarse en grandes masas en el Plano Astral y su energía ser precipitada al plano físico.

Nosotros originamos **causas**, y éstas despiertan los poderes correspondientes del Mundo Sideral, los cuales son magnética e irresistiblemente atraídos hacia los que han producido tales causas y reaccionan sobre ellos, ya sea que éstos verifiquen el mal materialmente, ya sean simples pensadores que engendran daños mentales.

En todas direcciones y de innumerables modos, causan estragos los pensamientos malos de los hombres, y el que debiera haber sido cooperador divino en la obra de Universo, emplea en la destrucción sus poderes creadores.


## <span id="karma14">[Conclusión](#karma14)</span>
El conocimiento de la gran Ley de Karma puede acelerar la evolución del hombre; la ineludible cosecha se da ahora o en el porvenir.

"La fuerza de una creencia, se mide por su influencia en la conducta." profesor Baín.

La Naturaleza no puede esclavizar al Alma que ha obtenido el Poder por medio de la Sabiduría, y emplea ambos en el Amor.


---


## <span id="dharma1">[CAPÍTULO I: Las diferencias](#dharma1)</span>
Al nacer, Dios le entrega a cada nación una palabra especial que debe profesar como su contribución para una humanidad ideal y perfecta, para la India fue "Dharma".

Se menciona la historia de la batalla de Kurukshetra, el héroe Brishmâ que fue la más alta personificación del Dharma, el lecho de flechas donde reposaba, se acercan los vencedores hijos de Pându con Yudhisthira a la cabeza, la divinidad alivia a Brishma para dar aquel gran discurso, que entre los temas tratados abordaba el de la moralidad.

Misterioso es el sendero de la acción, donde el bien y el mal son relativos en un universo de condiciones variables; se menciona el Dharma concerniente a las cuatro castas.

Se da la primera mitad de la definición de Dharma, se mencionan las tres partes en que se dividrá el tema para abordarlo: las Diferencias, la Evolución, el Bien y el Mal.


## <span id="dharma2">[CAPÍTULO II: Las diferencias (Segunda parte)](#dharma2)</span>
Se menciona la perfección de un Universo, Ishvara y Mâyâ, la Belleza impresa por Ishvara, la relevancia de la variedad y diferencias, las tres cualidades o Gunas: Sattva (armonía, placer), Rajas (energía, movimiento) y Tama (inercia, estabilidad); se menciona que en todos hay una sola vida inmortal, eterna, infinita, aunque se manifiesta en diferentes grados de evolución y diferentes períodos de desenvolvimiento. Se retoma la definición de Dharma con su segunda parte: la naturaleza en el punto alcanzado por el desenvolvimiento, *más* la ley conducente al período de desenvolvimiento que va a seguir; conociendo y siguiendo el Dharma propio (grado de desenvolvimiento y ley conducente) se llegará a la perfección, hay que entender cuál es el mejor tiempo y orden evolutivo relativos al Dharma individual, al Dharma se le llama ley o deber indistintamente, en un Universo condicional el bien y el mal no son absolutos sino relativos; las diferencias se manifiestan en la naturaleza, y son relevantes para entender el grado de evolución y la ley aplicada en cada caso.

La unidad no hace ninguna impresión sobre la conciencia. Las diferencias y la diversidad son las que hacen posible el desenvolvimiento de la conciencia. La conciencia no condicional escapa a nuestra comprensión.


## <span id="dharma3">[CAPÍTULO III: La Evolución](#dharma3)</span>
El orígen y semilla no desenvuelta de todas las cosas  del universo proviene de Dios, y de ahí se evoluciona sucesivamente. La vida oculta/dormida/en potencial empieza a despertar y activarse por las vibraciones y fuerzas exteriores que producen reacciones y vibraciones de la vitalidad interna. Se menciona la facultad de "la percepción". Las experiencias son importantes para, en un grado de evolución más avanzado, discernir el bien y el mal. Se mencionan los principios de la moralidad. Se menciona el período del *servicio* y los períodos siguientes, de adquisición de riquezas y frugalidad, de reyes y guerreros, de enseñanza.

el Dharma significa la naturaleza interior caracterizada por el grado de evolución alcanzado, *más* la ley determinante del crecimiento en el período evolutivo que va a seguir.

"La semilla de todos los seres"

La vida contiene todo potencialmente, pero nada manifestado de antemano. Contiene todo en germen, pero nada como organismo desenvuelto.

Él declara enseguida que es Su vida no manifestada la que impregna el universo.

El tiempo es nada para Ishvara porque Él es eterno y para Él todo ES.

El hombre, destinado a ser la imagen de su Padre, refleja en si mismo el Yo con el cual es uno y del cual emana.

Pero "la naturaleza" no es más que la vestimenta de Dios, Su manifestación más baja en el plano material.

Toda experiencia es útil.

Asi se ha alcanzado un grado más: la respuesta emitida por la vida oculta atravesando la envoltura.

Todo lo que procura placer es armónico. Todo lo que hace sufrir es una disonancia.

Lo que es cierto en música es cierto en todo.

Establecer las distinciones es la única manera que tiene nuestra conciencia, por el momento al menos, para llegar a distinguir los objetos entre ellos.

... se crea un *centro* hacia el cual todo converge desde fuera y desde el cual todo diverge hacia el exterior.

la esperanza del placer antes de que el contacto tenga lugar.

Él desea el placer e impulsado por el intelecto, se dedica a su búsqueda.

Volvamos, sólo por un instante, al animal. ¿Qué es lo que le impulsa a la acción? El deseo imperioso de librarse de una sensación desagradable.

El grado de evolución de la conciencia puede establecerse por la relación existente entre las influencias determinantes exteriores y los móviles espontáneos.

La conciencia inferior es impulsada a la acción por influencias exteriores a ella misma. La conciencia superior es impulsada a la acción por móviles que provienen de adentro.

"Tratar de librarse de los deseos satisfaciéndolos, es pretender extinguir el fuego, con manteca derretida. Es preciso humillar y dominar el deseo. Es preciso sofocar en absoluto el deseo."

En las primeras fases, la satisfacción de los deseos es la ley de la evolución.

Necesario es comprender que, en este período, no existe nada que pueda llamarse moralidad. No hay distinción entre el bien y el mal. Todo deseo debe ser satisfecho.

La satisfacción de este deseo es la ley de su progreso. El Dharma del salvaje es pues el satisfacer todos sus deseos.

La experiencia es la ley de la vida y del progreso.

Estos opuestos se presentan a la conciencia en un momento dado bajo la forma de bien y mal.

El recuerdo de una orden confirmada por la experiencia hace sobre la conciencia una impresión mucho más fuerte y más rápida que la experiencia sola sin la revelación de la ley.

Esta declaración de lo que los sabios califican de principios fundamentales de la moralidad --a saber, que ciertos géneros de acción retardan la evolución y otros la aceleran-- es, para la inteligencia, un inmenso estimulante.

El Dharma, recordadlo, comprende dos elementos: la naturaleza interior en el punto a que ha llegado y la ley que determina su desenvolvimiento en el período que se va a abrir ante ella.

El primer Dharma es el del *servicio*.

La facultad de actuar con independencia queda ahora muy restringida.

En nuestros días reina la confusión.

El hombre que tiene por Dharma el servicio, debe obedecer ciegamente a quien sirve.

Se vacila en admitir esta doctrina, pero es verdadera.

Su desarrollo no debe ser detenido por nuestras durezas, como sucede generalmente.

Acumular con energía y gastar con cuidado, discernimiento y liberalidad, tal es el Dharma de un Vaishya, la manera como se manifiesta su naturaleza y la ley de su crecimiento ulterior.

Pues que este cuerpo desaparezca y que el alma quede libre para volver a tomar otro cuerpo nuevo que le permita manifestar más altas facultades.

Nuestros cuerpos, que vemos aquí, pueden perecer periódicamente, pero cada muerte es una resurrección a una vida superior. El cuerpo en sí no es más que una vestidura en que el alma se envuelve.

Entonces, que el cuerpo desaparezca. Tal es la difícil lección que aprende el Kshatriya.

El hombre ha llegado a este grado de evolución en que la expansión natural de su naturaleza interior le impulsa a instruir a sus hermanos más ignorantes.

No debe perder jamás el imperio sobre sí mismo. Jamás debe ser arrastrado. Siempre debe dar prueba de dulzura.

Debe desprenderse de los objetos terrestres si ejercen alguna acción sobre él.

... y el objeto de este es la liberación.

Colocad vuestro propio ideal tan alto como sea posible, pero no lo impongáis a vuestro hermano, pues la ley de su crecimiento puede ser enteramente diferente de la vuestra.

Aprendéd el significado del Dharma y podréis ser útiles al mundo.

Tomad por objetivo aquello que podáis imaginar de más sublime en el pensamiento y en el amor; pero al tomar este objetivo, tened en cuenta los medios, lo mismo que el fin, vuestras fuerzas y vuestras aspiraciones.

Pero es necesario tener la tolerancia del que sabe y la paciencia que es divina.


## <span id="dharma4">[CAPÍTULO IV: El bien y el mal](#dharma4)</span>
Se habla sobre el orígen de la moral y se mencionan las tres escuelas de moral de Occidente; las escuelas tienen sus bases de moral como: la revelación de Dios, la intuición y la utilitaria. Se da la definición de bien y mal, de moral e inmoral con ejemplos, se concluye que la moralidad es relativa. Se habla de los crímenes, los criminales y los tipos de criminales. Se examinan algunas faltas, distinción entre el matar del asesino y el de la guerra; se habla de la *Separatividad* y la unidad. Se mencionan las enseñanzas y conductas de los Sabios, se menciona un fin supremo.

El bien absoluto sólo existe en Ishvara.

Una de estas escuelas dice que la revelación de Dios es la base de la moral.

Tiene por consiguiente algo de quimérico y nos da el presentimiento de que no es razonable permitir hoy lo que era permitido a una humanidad en su infancia.

Otra escuela ha nacido dando la intuición como base de la moral y diciendo que Dios habla a cada hombre por la voz de su conciencia.

Esta experiencia que se pierde en la noche de los tiempos, le permite juzgar hoy tal o cual línea de conducta. La llamada intuición es el resultado de infinitas encarnaciones.

La tercera escuela de moral es la utilitaria.

Una sola vida es su raíz, un solo Dios es su fin.

Este sistema desconoce la unidad inviolable de la raza humana y por lo tanto, su máxima no puede servir de base a la moral.

... el mártir es el mayor mentecato que ha producido la humanidad, porque deja escapar todas las probabilidades de bienestar sin recibir nada a cambio.

Teóricamente la revelación es mirada como la autoridad y en la práctica se hace abstracción de ella porque resulta bastante imperfecta. Consecuencia absurda: aquello que es declarado autoridad es rechazado en la vida y el hombre lleva, con poca fortuna, una existencia ilógica, sin ton ni son, sin tener por base ningún sistema preciso y razonable.

Esta evolución tiene su causa en la voluntad divina. Dios es la potencia motriz, el espíritu director del conjunto.

El bien es aquello que trabaja de acuerdo con la voluntad divina, en la evolución del Universo, e impulsa esta evolución en su marcha hacia la perfección, El mal es aquello que retarda o impide la realización de los designios divinos y tiende a hacer retrogradar el Universo hacia un grado inferior a aquel a que le conduce la evolución.

El bien es lo que contribuye a la evolución hacia la divinidad; el mal es lo que la hace retroceder y retarda su marcha.

Entonces, si elegís el mal camino, pecáis, violáis la ley que ya conocéis y admitís.

La experiencia del mal es necesaria solamente *antes* de que el mal sea reconocido como tal y con el fin de que pueda serlo.

... es una traición a nosotros mismos permitir que el bruto que está en nosotros se sobreponga al Dios que está en nosotros.

En el primer caso el móvil es egoísta, en el segundo procede de un yo más amplio, el yo nacional.

El crimen reaparece bajo formas sorprendentes, disfrazado y oculto y los hombres deben aprender vida tras vida, a purificarse a sí mismos.

Tales son los crímenes inevitables que a veces arruinan una bella existencia, en el momento en que los pensamientos de otras veces dan sus frutos en el presente y cuando el Karma del pensamiento acumulado se manifiesta en acción.

El instante de reflexión significa que podéis poner vuestro pensamiento en el lado opuesto y reforzar así la barrera.

En este caso el pensamiento pertenece al pasado y la acción al presente.

Las almas, en un momento dado, necesitan ser egoístas.

La conciencia pública empieza a reconocer que no es la separatividad, sino la unidad, la que permite el verdadero desenvolvimiento de una nación.

Esta simboliza el descenso en la materia y la unificación la subida hacia el espíritu.

El mal hoy tiene su orígen en la separatividad.

¿Nuestra acción presente tiene por objeto una ventaja personal, o el bien general?

Tendréis una noción de las maravillas de los misterios del Universo cuando sepáis que lo que parece mal, visto desde el lado de la forma, es bien, visto desde el lado de la vida.

Todo lo que viene es para el mayor bien del mundo.

He aquí la verdadera manera de considerar la naturaleza.

Un crimen es menos pernicioso para el alma que la idea fija contínua, que el desarrollo de un cáncer en el centro de la vida.

... dejadme pediros que escuchéis el pensamiento que está en el mensaje y no las palabras del mensajero...


## <span id="referencias">[REFERENCIAS](#referencias)</span>
* [https://archive.org/details/BSG_8RSUP5047/mode/2up][1]
* [https://en.wikipedia.org/wiki/Annie_Besant][2]


[1]: https://archive.org/details/BSG_8RSUP5047/mode/2up
[2]: https://en.wikipedia.org/wiki/Annie_Besant
