---
title: GameBoy clips
date: "2023-11-13T06:53:47.121Z"
description: "Some GameBoy emulator games clippings."
tags: GB GameBoy GBC GameBoyColor clips game games VideoGame VideoGames Nintendo emulator
image: ./Game_Boy_Color_logo.svg.png
---


## <span id="PokemonYellow">[Pokémon Yellow](#PokemonYellow)</span>

![PokemonYellow_01](./PokemonYellow_01.jpg)

![PokemonYellow_00](./PokemonYellow_00.jpg)

![PokemonYellow_Intro2](./PokemonYellow_Intro2.jpg)

![PokemonYellow_Intro1](./PokemonYellow_Intro1.jpg)


## <span id="ZeldaLinksAwakening">[The Legend of Zelda: Link's Awakening](#ZeldaLinksAwakening)</span>

![ZeldaLinksAwakening_00](./ZeldaLinksAwakening_00.jpg)

![ZeldaLinksAwakening_Intro5](./ZeldaLinksAwakening_Intro5.jpg)

![ZeldaLinksAwakening_Intro4](./ZeldaLinksAwakening_Intro4.jpg)

![ZeldaLinksAwakening_Intro3](./ZeldaLinksAwakening_Intro3.jpg)

![ZeldaLinksAwakening_Intro2](./ZeldaLinksAwakening_Intro2.jpg)

![ZeldaLinksAwakening_Intro1](./ZeldaLinksAwakening_Intro1.jpg)


## <span id="references">[REFERENCES](#references)</span>
* [https://en.wikipedia.org/wiki/Game_Boy][1]
* [https://en.wikipedia.org/wiki/Game_Boy_Color][2]


[1]: https://en.wikipedia.org/wiki/Game_Boy
[2]: https://en.wikipedia.org/wiki/Game_Boy_Color
