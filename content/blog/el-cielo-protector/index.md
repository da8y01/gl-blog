---
title: El cielo protector - Paul Bowles
date: "2025-01-06T18:08:12.121Z"
description: "El libro 'El cielo protector' del autor estadounidense Paul Bowles."
tags: libro EstadosUnidos ElCieloProtector_PaulBowles ElCieloProtector PaulBowles
---


![ElCieloProtector_PaulBowles_Cover](./image-placeholder-icon-7.jpg)


## <span id="PrimeraParte">[PRIMERA PARTE. Té en el Sahara.](#PrimeraParte)</span>
Se presentan a Port, Kit y Tunner, viajeros (no turistas) que recorren ciudades y pueblos de África después de la guerra. Port despierta en una habitación de hotel de una ciudad africana, luego en el café se encuentra con su mujer Kit y el amigo Tunner, hablan, Port cuenta su sueño, conocen a los ingleses Lyle (Eric y la madre). El viaje a Boussif lo hace Port en coche con los Lyle, y Kit y Tunner en tren en donde tienen relaciones íntimas, luego el viaje a Aïn Krorfa se hace en camión, las moscas anuncian la cercanía de la ciudad, en Aïn Krorfa Port ve la oportunidad de organizar las cosas para despachar a Tunner con los Lyle hacia Messad, y continuar solo con Kit hacia la ciudad Bou Noura.


## <span id="SegundaParte">[SEGUNDA PARTE. El borde afilado de la tierra.](#SegundaParte)</span>
En Bou Noura el Teniente d'Armagnac se entera de que un extranjero norteamericano ha perdido su pasaporte. Pasan algunos días, se mencionan algunos primeros síntomas que insinúan que Port se está enfermando, se planea y ejecuta el viaje hasta la ciudad El Ga'a, con la ayuda de un árabe Kit deja reposando a Port en un establo, en El Ga'a no hay hospedaje y están en alerta por epidemia de meningitis, se logra conseguir un transporte informal hacia el pueblo de Sbâ donde hay un fuerte militar y algo de asistencia médica. En la ciudad de Sbâ el Capitán Broussard los recibe/acoge/ayuda, atienden con comprimidos (pastas) y sopa a Port, Kit lo cuida en el pequeño cuartucho del hospital/hotel/fuerte, Port en algunas pocas ocasiones reacciona/despierta/vuelve y habla a Kit. En Bou Noura Tunner se entrevista con el Teniente d'Armagnac, en Sbâ el Capitán Broussard hace un intento de acercamiento a Kit y le exige los pasaportes, un día en un paseo por los techos Kit ve que Tunner llega a Sbâ, se encuentran, hablan; cuando Kit vuelve al cuarto encuentra a Port muerto, se le escabulle a Tunner y a la cita/plan que tenían, Kit escapa, se resguarda debajo de un árbol tamarisco, Port es enterrado patéticamente en el cementerio detrás del fuerte al modo católico con una cruz sobre su tumba; de vuelta en Bou Noura Tunner habla con el Teniente d'Armagnac, se encuentra con los Lyle y cae en cuenta que Eric Lyle había sido el ladrón del pasaporte de Port y lo golpea, a la mañana siguiente los Lyle han partido.


## <span id="TerceraParte">[TERCERA PARTE. El cielo.](#TerceraParte)</span>
Después de escaparse/escabullirse del cuartucho de Sbâ Kit se une a una caravana de camellos del desierto, el jóven árabe Belqassim la toma como su mujer, después del recorrido Belqassim hospeda a Kit en un cuarto de su casa camuflada/disfrazada como a un joven viajero enfermo al que ha brindado ayuda; las esposas locales de Belqassim junto con el niño Othman y las escalvas espían y descubren a Kit y a Belqassim, la maltratan, le dan latigazos y golpes, Belqassim la toma como esposa, le entrega las joyas de las otras esposas, le dan/administran la bebida somnífera, Belqassim ya no la visita constantemente, una noche Kit se escapa de la habitación y de la escalva negra durmiente para dirigirse a la salida hacia la ciudad, una gran puerta aherrojada le cierra el paso, al dirigirse a una casa con luz toca y se da cuenta que es la de las esposas, intercambia con ellas, les muestra lo de la maleta, les da a entender que el resto de las joyas están en el otro cuarto, y finalmente logra que la ayuden a salir/dejar de la ciudad. Al día siguiente vuelve a la ciudad, al tomar un pote de quefir y tratar de pagar no le aceptan su dinero en francos, llega un hombre llamado Amar que intercede por ella, paga lo adeudado a la mujer negra vestida de blanco, se la lleva de aquella plaza, llegan a una especie de posada donde los recibe una mujer proxeneta que los rechaza, Amar la lleva a un café, luego en el techo recuerda que debe enviar un telegrama, habla con Amar, la lleva a una de las habitaciones del café de Atallah. Al final Kit acompañada por Amar y una monja en un hospital, es enviada y transportada a una ciudad capital por intercesión/intervención del consulado, la recibe Miss Ferry que le da informes preliminares incluyendo las comunicaciones con Tunner; al llegar a la ciudad y arribar en un taxi, mientras Miss Ferry se reporta en recepción, ella se pierde de vista estando en la ciudad y el centro a donde habían llegado.



## <span id="referencias">[REFERENCIAS](#referencias)</span>
* [https://es.wikipedia.org/wiki/El_cielo_protector][1]
* [https://es.wikipedia.org/wiki/Paul_Bowles][2]
* [https://www.paulbowles.org/][3]


[1]: https://es.wikipedia.org/wiki/El_cielo_protector
[2]: https://es.wikipedia.org/wiki/Paul_Bowles
[3]: https://www.paulbowles.org/
