---
title: El sentido común - Yoritomo Tashi
date: "2024-05-01T18:08:12.121Z"
description: "El libro 'El sentido común' del autor japonés Yoritomo Tashi."
tags: libro japón ElSentidoComún YoritomoTashi
---


![ElSentidoComun_YoritomoTashi_Cover](./image-placeholder-icon-7.png)


## <span id="prefacio">[PREFACIO](#prefacio)</span>
Su reino es de este mundo y sus teorías se refieren más a la felicidad tangible que a la de ultratumba.

El comentarista expresa la satisfacción de volver a encontrarse con el autor, hace referencia/mención al estilo/aire del autor y su obra.


## <span id="leccion01">[Lección Primera. -- El sentido común tal como debe comprenderse](#leccion01)</span>
**Glosario:** arabescos, incuria, ufana, circunspecto, *daimio*, numen, *Shogún*, *Taikún*, *mikado*, *kaquimono*, corolario, emblema, serena.

**Citas:**

El sentido común, por su parte, casi nunca se ve forzado a combatir; crece en paz, lejos del tumulto de las dificultades y de las tupidas malezas.

Su flor es menos bella que otras muchas; pero jamás se deja eclipsar por el desorden de las plantas vecinas.

Un absurdo prejuicio presenta al sentido común como una forma inferior de la inteligencia.

Los que cultivan el sentido común reconocen francamente sus yerros, si bien es cierto que rara vez están apartados de la verdad, puesto que proceden con rectitud de espíritu y se esfuerzan por mantener su línea de conducta.

Es el sentido central hacia el cual convergen todas las impresiones, que se confunden en un sentimiento único: el deseo de la verdad.

El amor de la rectitud y de la simplicidad.

El sentido común permite eludir el temor que se apodera siempre de aquel cuyo razonamiento vacila; evita los desfallecimientos de la voluntad e infaliblemente indica la actitud que procede adoptar.

El sentido común varía según las costumbres y el ambiente.

El sentido común puede rechazar o desear una cosa, sin que por ello sufra la lógica en lo más mínimo.

EL sentido común --dice-- es el arte de resolver los problemas, no el de plantearlos.

El sentido común influye raramente en una inicativa.

Es el elemento principal del discernimiento.

El sentido común es la síntesis de numerosos sentimientos que concurren a formarlo.

En primer lugar mencionaremos el raciocinio.
Viene luego la moderación.
Enumeraremos, además:
El espíritu de penetración.
La continuidad del esfuerzo.
La cordura, que permite aprovechar las lecciones de la experiencia.

El raciocionio es indispensable para determinar el alcance de los buenos pensamientos.

El raciocionio es el arte de establecer la relación de las cosas.

La práctica del raciocinio nos permite discernir lo que conviene efectuar y lo que debe desecharse.

La vida está llena de celadas, como una selva misteriosa.

... en una palabra; es hacer acto de voluntad consciente, y esto constituye una de las fases más esenciales de la conquista del sentido común.

Sin raciocinio no hay sentido común.

[La moderación] Nos enseña a contener la impaciencia, a acallar las antipatías mal definidas y a poner un freno a los entusiasmos intempestivos.

[*El don de penetración*] De todas maneras, es preferible circunscribir este análisis a los hechos que no nos atañen directamente; de tal modo nos acostumbraremos a juzgar sin pasión y sin interés personal.

[*La continuidad del esfuerzo*] Se inspira en el raciocinio, no puede existir sin moderación e implica cierta penetración, pues quiere obrar bajo el imperio del convencimiento.

Tiene grandes semejanzas con la perseverancia; pero no puede confundirse, sin embargo, con esta virtud, pues sus atribuciones proceden sobre todo de la lógica y el raciocinio, que no siempre producen un acto, sino una serie de actos, dependientes algunas veces, corolarios siempre.

*La cordura* --prosigue el filósofo-- sólo debe mencionarse aquí como fórmula que permite analizar la experiencia.

De esta lección perpetua que nos da la vida se forma la cordura, la "sabiduría" de casi todos los ancianos.

Se preguntará si es verdaderamente necesario llegar al límite de la decrepitud para aprovecharse de una experiencia ya inútil, como una conquista póstuma.

Es muy cierto que el que tiene memoria de una vida larga, debe conocer mejor que un joven los peligros y emboscadas de que está sembrada.

Además, el anciano no puede juzgar los sentimientos que experimentaba a los veinte años, a no ser que recurra a reminiscencias más o menos fugitivas y a una intensidad de representación sumamente atenuada.

Como la percepción emotiva es muy débil, es natural que la fidelidad de los recuerdos no sea completa ni mucho menos.

Así, pues, aunque honremos la cordura de los viejos, es conveniente procurar adquirirla cuando puede sernos todavía de un auxilio precioso.

... fijándose en la ventaja que ofrece juzgar las cosas sin sentimiento personal alguno.

Todo el mundo puede tener cordura, si se llega a comprender bien que la experiencia ajena puede ser de tanta utilidad como la propia.

... aparecerá el sentido común, tranquilo y temible, bienhechor, como todo lo que es emblema de la fuerza serena.

**Sinopsis:**

Empieza exponiendo el concepto y características del sentido común con una analogía/comparación con un jardín, flora y maleza; enumera y describe las cualidades y sentimientos que concurren a formarlo.


## <span id="leccion02">[Lección Segunda. -- La lucha contra la quimera](#leccion02)</span>
**Glosario:** *Shogún*, inveterada, apatía, augusta, simpático.

**Citas:**

Tal como acabamos de describir el sentido común según Yorimoto, vemos que constituye la antítesis de la imaginación romántica; es el enemigo jurado de la quimera, con la que lucha al punto que la divisa.

El sentido común es fuerte y la quimera, débil...

Pronto renunciará a la lógica, por resultar sus conclusiones contrarias a sus deseos, y caerá en la dulzura falaz que proporciona el soñar despierto.

Los que cultivan el sentido común no conocen nunca el abatimiento que causa la pérdida de las ilusiones.

No hay nada más estúpido que un sufrimiento voluntario cuando es infecundo.

La afición a lo maravilloso no es, pues, la única.

La pereza influye en ello casi siempre.

¡Es tan dulce pensar en una solución conforme a lo que se desea!

Comiénzase por desear ardientemente la realización que se espera; luego viene la costumbre de tomar el deseo por realidad y con ayuda de la pereza se tiene por hecho el triunfo fácil.

Se goza por adelantada de un triunfo que se imagina conseguir y que se considera indiscutible gracias a la ligereza de ánimo.

También entra en ello la credulidad, a la que, a nuestro entender, se concede un puesto muchísimo más elevado que el que merece.

La credulidad, cuando no proviene de una tontería inveterada, es siempre el resultado de la apatía y de la debilidad.

Su defecto les privará de la satisfacción que causan los esfuerzos bienhechores; serán víctimas de la doblez y del engaño.

... pues la desaparición de la quimera había disipado su ánimo y les había herido con el hierro de la desesperación.

Yoritomo piensa, con razón, que la quimera podría muchas veces convertirse en agradable realidad si se comprendiera mejor y si en vez de considerarla a través de los sueños de la imaginación se la despojara de los engaños que la envuelven para vestirla con la recia envoltura del sentido común.

El que sabe observarse detenidamente podrá evitar las desilusiones que por las sendas del error conducen a la desesperación.

Hay que considerar el culto de la quimera como el verdugo de la esperanza.

Casi todos los escépticos son amantes inconfensos de la quimera; como sus deseos no pueden jamás realizarse, han perdido la costumbre de esperar un desenlace favorable.

Examinar ordenadamente los proyectos que abrigamos, es preparar su feliz terminación.

Constituye el único medio para despojarles de la seductora quimera que devora las ambiciones sanas y las aspiraciones de la juventud.

A esa suerte de imaginaciones puede aconsejarse el cultivo del ensueño en su forma más augusta: la de la ciencia.

La quimera puede ser bella, magnífica, a condición de que no sea siempre esquiva.

Entonces la quimera toma otro nombre: se llama ideal.

En vez de ser un mito inaccesible, se convierte en bienhechora entidad.

Compadezcamos a los que viven por una quimera como a los que no han sabido crear un ideal imaginario, cuya belleza sirve de faro para encauzar los esfuerzos.

El triunfo del sentido común consiste en operar esta transformación y desechar los sueños vanos, para convertirlos en deseo de perfección. Así pueden alimentarse esas iluisones sin que sea menester destruirlas.

Las divagaciones cederán el puesto al raciocinio, y la terrible desilusión, derrotada, correrá la suerte de la quimera, dejando brotar ufana la flor de la esperanza, inundando de paz y armonía las almas serenadas para siempre, gracias a la razón y al sentido común.

**Sinopsis:**

Se expone que el sentido común es enemigo o se contrapone a la imaginación romántica, la quimera y las ensoñaciones, la importante energía se aminora y la pereza influye, persuade y desvía; se expone la comparación entre los pesados caminantes y los pajareros simpáticos; se expone el abatimiento causado por la pérdida de ilusiones, y la cohartación de la esperanza debido a esto, la vanidad y la credulidad aliadas a la pereza también contribuyen a alimentar las malsanas ilusiones, y a acabar la esperanza; se menciona la anécdota de los "perseguidores de quimeras"; se expone cómo tratar la quimera usando el sentido común para rescatar la esperanza; se menciona que los escépticos son amantes o afines a la quimera; se expone que para cierto tipo de imaginaciones e ilusiones se puede cultivar/aplicar/practicar la ciencia para convertirlas en ideal y deseo de perfección.


## <span id="leccion03">[Lección Tercera. -- El culto del raciocinio](#leccion03)</span>
**Glosario:** estatuir, incuria, aciago, raciocinar, pueril, erudición, derogada, arrogante, efímera.

**Citas:**

En la Humanidad nada acaba del todo; cada cosa se detiene para volver a empezar.

El raciocinio --dice-- es una divinidad, en torno de la cual gravitan infinidad de dioses importantes, aunque inferiores.

El raciocinio obedece a ciertas leyes que se condensan todas en un sentimiento único: el sentido común.

La memoria es una resurrección parcial de la vida pasada, cuya reconstitución nos permite dar su verdadero valor a las fases de la existencia actual.

Permite ejercer su juicio al espíritu, sin dejarse influir por la magnificencia o la excesiva humildad de la idea.

Por regla general, un pensamiento provoca casi siempre la aparición de pensamientos similares que vienen a agruparse en torno a la idea primera, como los pájaros de igual casta, que dirigen el vuelo hacia el mismo sitio.

[El pensamiento] Es lo que les diferencia de los animales, que no poseen otra guía fuera del instinto.

La inercia moral es el más deplorable de los defectos; dificulta el curso de la vida intelectual y el desarrollo de la personalidad.

Es por eso la enemiga del sentido común, pues admiitará fácilmente un razonamiento ya hecho antes que decidirse al esfuerzo necesario para descubrir la verdad y formarse una opinión personal.

Vemos, pues, que casi siempre la vulgaridad constituye el signo de la pereza mental.

No debe confundirse, sin embargo, la prevención con la previsión, confusión en que tantos incurren.

La prevención consiste en el cuidado de impedir la repetición de hechos aciagos que han ocurrido ya.

La previsión es pasiva; entre ella y la prevención existe la misma diferencia que entre la teoría y la práctica. Por el contrario, la prevención es esencialmente activa.

Comienza por la previsión; pero no se detiene ahí sino que continúa el esfuerzo hasta hacerlo productivo.

Bueno es prever; pero es mucho mejor impedir.

... pero raciocinar según los principios de la justicia y de la verdad es una operación que sólo pueden llevar a buen término los espítitus dotas de sentido común.

Ante todo, hay que evitar contentarse con hacer las cosas a medias, lo cual lleva consigo infinidad de peligros inesperados.

... Sería pueril darles una importancia capital; pero se lamentaría indudablemente el haber despreciado completamente su advertencia.

Algunas veces, el exceso de erudición entorpece el raciocinio basado en el sentido común.

Existen verdades escenciales que la vida cotidiana modifica, sin que por ello las despoje de su autoridad. Las hay prematuras, como las hay derogadas.

[El juicio] Es el indicio de la certeza del espíritu.

Esta operación mental consiste en comparar varias ideas para determinar su relación.

Sigue inmediatamente al raciocinio, del cual es resultado.

Todo juicio es afirmativo o negativo.

No puede ser indeciso ni neutral.

En este último caso, adoptaría el carácter de una opinón y perdería la cualidad definitiva que lo caracteriza.

Algunas veces, sin embargo, es condicional, cuando los principios sobre los cuales se basa no son lo bastante definidos y por este hecho pueden ser objeto de conversión.

Entonces, sin faltar a las leyes del sentido común, puede sentirse un juicio cuyos términos se modificarían por la mutación de las causas.

El sentido común exige que todas estas influencias diferentes se prevean y que se mencionen todas las eventualidades al pronunciarse el juicio.

De este modo ha querido demostrarnos cuán aminoradas serían estas cualidades si no estuvieran reunidas y enlazadas en el orden que deben manifestarse.

... pues el sentido común es la inteligencia de la verdad.

**Sinopsis:**

Se presenta "el abanico simbólico del raciocinio", sentido común: percepción, memoria, pensamiento, actividad mental, deducción, prevención, raciocinio, juicio. Se describe el proceso/secuencia de las cualidades que concurren a los progresos del conocimiento; se explica la importancia del orden y enlaces del abanico simbólico, y las consecuencias/resultados de perder alguna varilla o el punto de unión que es el sentido común.


## <span id="leccion04">[Lección Cuarta. -- El sentido común y los impulsos](#leccion04)</span>
**Glosario:** injuria, *crisantemos*, férula, *daimio*, paroxismo, furor, terquedad, pueril, atonía.

**Citas:**

La substancia comprende todas las materialidades corpóreas, necesidades instintivas, gestos irreflexivos; en una palabra, todas las acciones en que no interviene el sentido común.

La esencia es la parte imponderable que comprende el alma, el espíritu, la inteligencia, la mentalidad, en suma.

Algunos confunden la intuición con el impulso. Estas dos cosas, muy distinas, sólo coinciden en la espontaneidad del pensamiento que las engendra.

La intuición es una sensación completamente anímica, especulativa; el impulso es práctico y se resuelve siempre por actos y resoluciones.

[El impulso] Es el resultado de la falta de dominio de sí mismo, que sólo pueden corregir la voluntad y el raciocinio.

La intuición es menos espontánea que el impulso.

El impulso, por el contrario, sólo acepta el instinto.

La inspiración --dice-- es muy raro que se inmovilice en la forma que adoptó al presentarse.

Casi siempre también los impulsos obedecen a las sensaciones corporales.

El raciocinio definitivo y el juicio imparcial inspirados por el sentido común, son rara vez patrimonio de un hombre enfermo.

[El hombre enfermo, que sufre] Antes de discutir las ventajas de la lucha resolverá instintivamente continuar con la inercia.

[El impuslo] ... multiplica las manifestaciones irreflexivas y produce el derroche de fuerzas que habrían de concentrase en un punto central, después de pasar por todas las fases de que ya hemos hablado.

[Los impulsivos] Su inclinación les arrastra hacia las soluciones instantáneas y desconcen las ventajas de la continuidad del esfuerzo.

... el furor se trocó en atonía, pues los impulsivos son incapaces de resistir un obstáculo mucho tiempo.

Con arte delicadísimo hizo recorrer al impulsivo toda la cadena de sentimientos que llevan de la percepción al juicio.

... la dificultad hace siempre reflexionar a los instintivos.

Nuestra época febril nos hace víctimas de la impulsión.

**Sinopsis:**

Se describen la características de los impulsos y los impulsivos, la substancia y la esencia, su afinidad con el instinto, su relación con la intuición, su conexión con la inspiración y el arte, su contraposición con el sentido común, se menciona la anécdota del campesino con la huerta, luego el autor expone cómo remediar o combatir los impulsos, se narra la historia del hombre impulsivo que el maestro Long-Ho logró curar con varias estrategias, incluyendo la del vestido de color amarillo de oro y verde intenso, y con mucho sentido común.


## <span id="leccion05">[Lección Quinta. -- Los peligros del sentimentalismo](#leccion05)</span>
**Glosario:** fútil, sensiblería, piedad, *daimio*, intendente, blasonar, oropel, pueril.

**Citas:**

Los que no hacen intervenir el sentido común en el exámen de su corazón son todos capaces de sentimentalismo, que para manifestarse no requiere que el objeto sea o no digno de él.

El sentimiento razona y se sacrifica.

El sentimentalismo excluye el pensamiento y no conoce la generosidad.

El sentimentalismo exige el sacrificio ajeno.

Así pues, se dirá: La abnegación es rara; luego el sentido común me indica que sería imprudente basar mi felicidad en la existencia de una cosa excepcional.

No hay nada que parezca más noble que el amor de los padres a sus hijos --dice--, y ningún sentimiento es más grande cuadno se comprende con toda su magnificencia.

Sin embargo, existen muy pocas personas capaces de aislarlo del sentimiento egoísta.

Otros se desvanecen al ver un accidente y no piensan en socorrer a los heridos.

Le demostré la evidente contradicción de su crueldad instintiva con el sentimentalismo de que blasonaba.

... el sentimentalismo no es más que un concepto del egoísmo en las diferentes formas que adopta.

Por odio instintivo al esfuerzo --dice--, los padres no riñen a sus hijos para no hacerles llorar.

... el sentido comín debiera hacerles comprender que es preferible hacer llorar a los niños que tolerar una deplorable propensión a los hábitos que más tarde habrán de causarles verdaderos sinsabores.

En el afecto paternal, como en la amistad o las emociones, el sentimentalismo no constituye otra cosa que la simplificación exagerada del *Yo*.

A pesar de los oropeles con que se presenta el sentimentalismo, será siempre el desecho del verdadero sentimiento.

**Sinopsis:**

Se exponen, con ejemplos y anécdotas, las diferentes clases de sentimentalismos comunes que hay: el obscuro llamamiento a la sensualidad, el que transforma la piedad real en sensiblería o exageración mental, el amor paternal, el de evitar el ejercicio de la piedad en el prójimo, el de apiadarse sobremanera de las cosas insignificantes que nos afectan de modo directo, el de los maestros que dejan que los niños sigan sus defectuosas inclinaciones por el deseo de no perturbar su propio sosiego.


## <span id="leccion06">[Lección Sexta. -- Utilidad del sentido común en la vida práctica](#leccion06)</span>
**Glosario:** cizaña, *Mikado*, digresión, fútil, pueril.

**Citas:**

Ningún hecho puede existir sin que haya un motivo suficiente para determinar su naturaleza.

Apoyándonos en los elementos proporcionados por el sentido común, llegaremos a discernir la cualidad de lo que es objeto de nuestra preocupación.

¿Os aconsejan quizá esas voces que nosotros no podemos oir, o acaso veis cosas que sólo a vós es dable percibir?

... En efecto, puedo ver y oir lo que vosotros no percibís; pero no hay en mi facultad ninguna brujería.

Abro los ojos y aguzo los oídos.

... después de haber visto y oído llamo en mi auxilio todas las cualidades que constituyen el sentido común y gracias a esta facultad saco conclusiones de mi experiencia, conclusiones en las que no intervienen ni el entusiasmo, ni la fantasía, ni el menor interés personal.

La naturaleza de las resoluciones inspiradas por el sentido común varía, pues, según el medio, la hora y el estado de espíritu en que nos hallamos.

Este método es precioso puesto que sucita las objeciones que no se presentarían al espítitu si se estudiase sólo la cuestión desde el punto de vista propio.

El que tiene sentido común puede pasarse sin ingenio y hasta sin audacia.

El sentido común suple a la inteligencia, sobre todo en las personas de alma poco sutil y capaces tan sólo de razonamientos simples.

Es amigo de la luz y marcha por el camino derecho, fuera de las malezas y de los supuestos atajos que pudieran entorpecerle.

El sentido común nos indicará la manera de conducirnos para no ofender los prejuicios.

El sentido común se aplica también a las cosas que se relacionan con la salud.

Sirve para preservar de los excesos, pesando las ventajas y los sinsabores que causan.

Es el médico más juicioso que podemos consultar.

Se pensaría que la enfermedad es una causa de desorden y falta de armonía sin igual.

El sentido común es la inteligencia del instinto.

**Sinopsis:**

Se explica que el sentido común influye en ciertos ámbitos cotidianos de la vida, la inutilidad de los procesos mentales y la nulidad de los exámenes realizados si el sentido común no interviniera; según lo acostumbrado, después de indicar el peligro da el remedio y la manera de aplicarlo; la influencia en aspectos de fé y religión, salud, deberes y responsabilidades.


## <span id="leccion07">[Lección Séptima. -- Poder de la deducción](#leccion07)</span>
**Glosario:** *daimio*, séquito, soberbio, palanquín, jayán, sinuoso, vaticinio, funesto, perspicacia, pueril, precepto.

**Citas:**

La operación mental más importante que debe ejercitar el que desee adquirir sentido común en todos sus actos y decisiones es, sin duda alguna, la deducción.

La asociación de ideas es un método excelente, pues introduce en el pensamiento agentes productores.

La deducción es siempre el resultado de varias observaciones formuladas claramente y que el sentido común une entre sí.

Hay que observar también el estado de salud, pues bajo la influencia de un malestar es muy fácil ver las cosas con hostilidad.

Sería, pues, injusto dar a los acontecimientos el alcance exacto si se han juzgado durante una enfermedad.

El filósofo, que piensa en todo, ha previsto el caso de que las deducciones se vean perturbadas por ideas falsas.

Los que tienen el hábito de la deducción asombran muchas veces a los impetuosos, por lo profundo de su juicio.

El filósofo nos previene, además, contra la propensión que tenemos de eludir casi siempre la conclusión que no está conforme con nuestros deseos.

Lo que se llama falsa deducción es casi siempre el deseo de eludir la resolciuón que dictaría la derechura de las apreciaciones.

Pudiera ser también que esta perturbación de juicio procediera de malas influencias sufridas anteriormente.

Lo importante --dice-- es no desviar el pensamiento, que después de considerar un instante el asunto que nos ocupa y las ideas similares, se complace en divagar sobremanera.

Muchas veces es tarde ya para volver al redil las ideas descarrriadas, pues en su desatentado vagar tocaron otras mil, que se confunden con la primera, la diluyen y le roban la claridad.

La deducción jamás prejuzga; se basa en los hechos; tan sólo desprende la enseñanza que debemos aprovechar de las cosas realizadas.

En la práctica de la deducción, el Shogún nos recomienda, además, la asociación de los pensamientos y el examen sucesivo de los incidentes anteriores.

Antes de hacer deducciones basadas en el aspecto general de las cosas, conviene examinarlas una a una y descubrir sus defectos y reconocer sus cualidades.

**Sinopsis:**

Se mencionan las operaciones mentales, ejercitación, fases de análisis del sentido común; se describe el método/proceso/secuencia de la deducción; el caso de las deducciones perturbadas por ideas falsas; la propensión a eludir conclusiones que van en contra de nuestros deseos; no desviar el pensamiento alejándose del punto central o perdiéndose en detalles o minuciosidades; asociar los pensamientos y el examen de incidentes anteriores.


## <span id="leccion08">[Lección Octava. -- Cómo se adquiere el sentido común](#leccion08)</span>
**Glosario:** armonía, sugestión, probidad, insensato, pusilánime, arrostrar, dúctil.

**Citas:**

Si no procuramos sustraernos a las sugestiones del estado presente llegaremos a transformar la percepción en concepto; es decir que no llegaríamos a la sola realidad sino a la realidad modificada.

El arte de la situación requiere aplicarse con extrema probidad, y esta es la razón de su importancia para adquirir el sentido común.

La forma está representada por los contornos diferentes.

La reflexión lo llevará luego a pensar que está cerca de una casa o de un ser, pues es raro que un perro se halle solo.

Los campesinos conocen los diferentes pájaros con sólo oirles cantar, y los solitarios, los errantes, los que viven en lucha constante con la sociedad, perciben sonidos que pasarían inadvertidos a los demás.

Referente al cálculo de probabilidades, el empleo de la aproximación nos permitirá apreciar la capacidad o duración probable de las cosas, basándonos en los conocimientos anteriores.

Existen, pues, límites conocidos que sirven de base a los razonamientos inspirados por el sentido común.

En la vida todo es cuestión de aproximación.

La aproximación, como la mayoría de los elementos que integran el sentido común, está basada en la experiencia.

El Shogún nos advierte acerca de la diferencia que existe entre la experiencia y la experimentación.

La experimentación --dice-- sólo sirve para provocar la aparición de la experiencia.

Con frecuencia la comparación se ejerce en nuestro espíritu de modo automático.

De todos modos existen cosas inimitables.

La música está compuesta de siete sonidos únicamente, cuya combinación forma una variedad infinita de melodías.

Se llama destinación al fin que nace de la deducción y de la clasificación.

La destinación no permite perder de vista el fin propuesto.

Existen costumbres --añade el filósofo-- cuyo origen no se recuerda; en un principio fueron hábitos creados por la necesidad; pero una vez desaparecido su objeto, la tradición las ha conservado, no obstante, y los que las practican no se dan cuenta de lo absurdas que son.

Los hombres de sentido común desprecian esas prácticas inútiles, y si las toleran junto a ellos es tan sólo por razones de utilidad o de decoración.

La conclusión definitiva se basa en la comparación, en la experiencia y, sobre todo, en la aproximación; es más que nada una síntesis de todos los elementos de que se compone el sentido común.

Luego se ejercitará, para poder juzgar por sí mismo del grado de razón a que puede aspirar.

Después se extenderá el juicio a cosas de más alcance; pero teniendo buen cuidado de mantenerse en los límites rigurosos de la lógica rudimentaria.

Las divinidades favorables no protegen más que a los cuerdos.

No hay nada más estéril que los remordimientos o los reproches, cuando no llevan consigo la resolución de no reincidir en la falta cometida.

El filósofo nos demuestra después la necesidad de prescindir completamente de la personalidad en los ejrcicios que concurren a la adquisicón del sentido común.

Tampoco querrá imitar a las personas pusilánimes que prefieren vivir en la angustia de la duda antes que arrostrar cara a cara los contratiempos.

Los que dudan carecen de serenidad.

Sin embargo, es prudente prever el resultado menos favorable y prepararse a sufrir sus consecuencias.

El hombre que así piense será más fuerte que la adversidad y sabrá luchar con buenas armas contra la fortuna contraria.

**Sinopsis:**

Las condiciones requeridas para cultivar el sentido común; el escollo impetuoso que es el razonamiento apasionado.


## <span id="leccion09">[Lección Novena. -- El sentido común y el sentido práctico](#leccion09)</span>
**Glosario:** fogosidad, vate, pugna, superstición, dimanar, simiente, energúmeno, insensato, envite, zamarrear.

**Citas:**

Mientras el sentido común se aplica a la totalidad de las circunstancias de la vida, el sentido práctico se ejerce sobre todo en las cosas utilitarias.

Llegaría algunas veces hasta el egoísmo si no recibiera el auxilio del sentido común.

La asociación del sentido común y del sentido práctico es necesaria para crear formas nuevas, limitando al propio tiempo la imaginación a las deducciones más precisas y al juicio más imparcial.

En cierto modo, la ciencia es el sentido común razonado y apoyado por argumentos que el sentido práctico encamina por el lado productivo.

Lo que se denomina sentido común, en su acpetación vulgar, es una cualidad que desarrolla el equilibrio mental cuando se trata de resolver asuntos materiales.

Su aptitud procede del hábito constante de la reflexión que, aguzando su atención, les permite observar detenidamente y deducir con fruto, cuando se trata de cosas que les son familiares.

La salud es el bien principal; puesto que sin la salud somos incapaces de apreciar los demás goces de la vida.

Si comprometo mi salud, de nada podré gozar.

Es una conexión estrecha entre la facultad de juzgar y la de deducir.

Facilita la coordinación, la claridad y precisión de los pensamientos.

El mal humor engendra la antipatía y la animosidad: de manera que es una torpeza muy grande provocar la aparición de estos sentimientos cuando se comprueba la existencia del mal humor.

La conformidad de los juicios se basa en el sentido común; pero el sentido práctico los determina.

Se limitará, pues, a evocar el sentido común, empleadno el sentido práctico en lo que respecta a los principios que desea imponer.

La superstición --dice-- ofende al sentido práctico como el sentido común, pues descansa en un análisis erróneo.

Muchas personas vuelven precipitadamente a su casa si al salir de ella han visto cierto pájaro; otras se creen amenazadas de muerte si tropiezan con un perro blanco.

De tal época remota proviene el orígen de la superstición relativa a la sal derramada.

La superstición, en todo caso, es la enemiga jurada del sentido común, pues cuando no dimana de una costumbre abolida, es fruto de una apreciación personal que asocia dos términos completamente ajenos entre sí.

Los espítitus superficiales están siempre dispuestos a sacar consecuencias de sus impresiones del momento; no admiten la prevención, ni mucho menos la aproximación.

El que juzga por las apariencias no razona; le falta lo que pudiéramos llamar la razón en la imaginación.

Cultivando el sentido práctico se evitará también el peligro de la idalización, que en las personas más equilibradas dora con frecuencia esta imagen con una falsa aureola.

**Sinopsis:**

Con ejemplos de razonamiento muestra la relación/conexión entre sentido común y sentido práctico; se recomienda la alianza entre los sentidos común y práctico para combatir la superstición; el sentido práctico impide juzgar según apariencias y superficialidades.


## <span id="leccion10">[Lección Décima. -- El mejor de los intermediarios](#leccion10)</span>
**Glosario:** quimera, presuntuoso, ambición, terco, abrojo, blasonar, apólogo, neurastenia, pueril, magín, pugna.

**Citas:**

Conócete a ti mismo.

... gracias al sentido común que las anima, poseen la ciencia de la verdadera proporción.

Únicamente las gentes que no tienen sentido común se rebelan contra las leyes del país donde viven.

La obediencia --dice-- debe considerarse como un medio; mas para el que desea encumbrarse no puede jamás ser tenida por virtud.

Esta obediencia pudiera calificarse de egoísmo si no estuviera marcada con el sello del sentido común.

No conviene nunca dar ejemplo de insubordinación, pues de seguirse no tardaría en surgir el desorden general.

El hombre que blasona de tener sentido común, si quiere prosperar es preciso que, sin perder su personalidad, refleje la de los que han de ayudarle a encumbrarse.

Queremos hablar de esa tendencia que sucesivamente tomó los nombres de hipocondría, pesimismo y, por último, de neurastenia, denominación que comprende todo género de enfermedades nerviosas, cuya característica es la tristeza incurable.

Sólo los locos quieren prolongar la permanencia en el lugar donde sufren toda suerte de martirios.

El hombre de sentido común, modestamente está siempre a cubierto de los caprichos de la fortuna...

**Sinopsis:**

El sentido común es un gran aliado, pero no basta conocer el concepto exacto de las cosas, es preciso, además, conocerse a sí mismo; de las leyes y la obediencia; el sentido común en lo tocante a negocios o a la sociedad debe ser camaleónico; de las enfermedades nerviosas consistentes en una tristeza incurable.


## <span id="leccion11">[Lección Undécima. -- El sentido común y el dominio de sí mismo](#leccion11)</span>
**Glosario:** infructioso, abnegación, loable, orgullo.

**Citas:**

Si todos observaran esta regla, muy pronto el bienestar individual crearía una armonía de la que todos los hombres disfrutarían.

Lo que se ha de evitar en bien de la tranquilidad general es el perpetuo conflicto entre el inetrés particular y el interés social.

Dominarse hasta el punto de no dejarse subyugar por las contingencias miserables que se erigen en tentaciones egoístas y ocultan con su puerilidad la belleza de la solidaridad, es dable tan sólo a unos pocos, y únicamente pueden comprenderlo los que tienen sentido común.

Por una mezquina realización que ven cerca son capaces de sacrificar grandes intereses que creen no les interesan de modo inmediato.

... El sentido común nos indica que hay que apelar al dominio de sí mismo para reprimir las tendencias que arrastran a los hombres a sacrificar el interés general en aras de la avidez personal.

Ésta gana muy rara vez con separarse de la masa, pues la prosperidad general es siempre la causa de las fortunas particulares.

Muy a menudo --dice-- se confunde el dominio de sí mismo con la libertad.

En esta ascención las guiará el sentido común, manteniéndolas en los lìmites prescriptos a las cosas de la razón y de la rectitud de espítitu.

En lugar de dejarse influir por las apariencias, había observado y reflexionado, y con atención llegó a una deducción basada en la verdad.

La condición esencial del dominio es la serenidad, que permite ver las cosas en su aspecto verdadero y nos impide dorarlas y ensombrecerlas según sea nuestro humor.

El miedo, el repugnante miedo no anida en el alma de los que poseen dominio de sí mismos y sentido común.

La sangre fría es el examen fugaz de las fuerzas físicas o intelectuales, con relación al peligro presunto.

Quienes poseen las facultades que acabamos de enumerar no tienen miedo, pues es muy cierto que desde el momento que la causa del temor se precisa, deja de ser temor y se convierte en ilusión estúpida o en el enemigo verdadero.

Tanto en uno como en otro caso no debe causar desasosiego sino desprecio o voluntad de luchar.

O el objeto de mi temor existe realmente o es una ilusión.

El dominio de sí mismo y la sangre fría son sobre todo necesarios para poder disimular las impresiones.

No conviene que el interlocutor lea el pensamiento del que habla con él, como en un libro abierto.

El hipócrita se esfuerza por aparentar emociones que no siente.

El hombre dotado de sangre fría consigue ser impenetrable.

Conocer las intenciones de un rival que ignora las nuestras es una de las condiciones esenciales del triunfo.

**Sinopsis:**

Se habla de la pugna/lucha entre el alma individual y el alma social; con ejemplos habla del dominio de sí mismo, la libertad y la sangre fría.


## <span id="leccion12">[Lección Duodécima. -- El sentido común con las aspiraciones](#leccion12)</span>
**Glosario:** calumnia, insensato, dimanar, pesquisa, aserto, silogismo, sofisma, mezquino, comulgar, proeza, temeridad.

**Citas:**

Sin él, el reinado de las cosas, aún las más admiradas, sería efímero; eso, si la falta de lógico no impidiera ya su producción.

El sentido común nos hará ver la diferencia que existe entre el juicio y la opinión, diferencia que casi nadie quiere analizar, ni siquiera admitir.

La opinión es una convicción que puede modificarse.

La integridad de juicio constituye uno de los dones más raros, pero al mismo tiempo el más noble que nos es dable poseer.

No tienen serenidad y viven en perpetua agitación, causada por la irritación que les domina sin cesar.

Además, el sentido común nos aconseja ser buenos, en bien de nuestro propio egoísmo.

No todas las almas son capaces de agradecimiento; el que hace bien puede esperar la ingratitud.

El sentido moral es el sentido común del alma...

El sentido moral nos indica hasta dónde llegan las concesiones permitidas y dónde empieza la licencia prohibida.

El hombre dotado de sentido moral vivirá tranquilo, ya que no conocerá la duda o, mejor dicho, la conocerá al tener la satisfacción de vencerla.

La belleza aparece sobre todo a los que cultivan el sentido común, desprecian los sofismas de la mentira y viven tan sólo de la verdad.

El entusiasmo es de cristal, pero el sentido común es de bronce.

**Sinopsis:**

Menciona la calumnia que es sugerir que el sentido común pertenece a una categoría de virtudes mundanas enfocada en la materialidad, y argumenta que es el fundamento/base/causa de la belleza y de nobles aspiraciones; se contraponen la opinión y el juicio; de la bondad; del sentido moral; el sentido común y las artes.


## <span id="referencias">[REFERENCIAS](#referencias)</span>
* [https://es.wikipedia.org/wiki/Yoritomo_Tashi#El_sentido_com%C3%BAn][1]
* [https://es.wikipedia.org/wiki/Yoritomo_Tashi][2]


[1]: https://es.wikipedia.org/wiki/Yoritomo_Tashi#El_sentido_com%C3%BAn
[2]: https://es.wikipedia.org/wiki/Yoritomo_Tashi
