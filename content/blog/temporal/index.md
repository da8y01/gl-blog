---
title: Temporal - Tomás González
date: "2024-03-29T16:06:22.121Z"
description: "La novela Temporal del autor colombiano Tomás González."
tags: libro novela colombia Temporal TomásGonzález
---


![Temporal_TomasGonzalez_Cover](./image-placeholder-icon-7.png)


## <span id="referencias">[REFERENCIAS](#referencias)</span>
* [https://es.wikipedia.org/wiki/Tom%C3%A1s_Gonz%C3%A1lez_(escritor)][1]
* [https://www.goodreads.com/book/show/18590762-temporal][2]


[1]: https://es.wikipedia.org/wiki/Tom%C3%A1s_Gonz%C3%A1lez_(escritor)
[2]: https://www.goodreads.com/book/show/18590762-temporal
