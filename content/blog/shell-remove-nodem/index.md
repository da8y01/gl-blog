---
title: Linux shell - Remove sub-directories
date: "2024-12-21T15:20:02.121Z"
description: "A linux shell script to remove node_modules directories."
tags: linux shell LinuxShell script
---


![LearningLinuxShellScripting_GaneshNaik_Cover](./LearningLinuxShellScripting_GaneshNaik_Cover.png)


```shell{numberLines: true}
#!/bin/bash

# rm_nodem_03.sh
# Remove node_modules/ directories
# 2024-12-21

target_dir="node_modules"*

for var in $(ls -d */)
do
    echo "Listing directory $var ..."
    for content in $(ls $var)
    do
        if [[ -d "$var/$content" && $content == $target_dir ]]
        then
            echo "$var$content found, removing ..."
            rm -rf $var$content
            # ls -d $var$content
            if [ $? -eq 0 ]
            then
                echo "OK"
            else
                echo "ERROR"
            fi
        fi
    done
    echo ""
done
```


## <span id="references">[REFERENCES](#references)</span>
* [https://www.amazon.com/Learning-Linux-Shell-Scripting-real-world-ebook/dp/B07BJKGHGT/ref=sr_1_6?crid=1KU74JQO76FXR&dib=eyJ2IjoiMSJ9.J02cXEdwrtnKliTUyt_Z16dV3o_fKaDqHPlxrNp5bFImeqwnbu3cEzJfD3T9Bqc1x6A0iDYZERvjohkT4CkYY4xBNJ54gIxHAkqSbf0zUk-ByIOwLHsLm3_adNTTwpSCjxpoUDyglTRWlgNfimLz1X1A5WZ34VTvWsoLHiYBxjlEc9cyOs8Hl3DgMe65iBDCOYMhFJSkrXmruWH8R0DPXBzRM5iu10h_8GgGdm0nCNk.0xJGCPtqA2StVzJ27zJpuX78DfuUaPnff29dKN2aS0Y&dib_tag=se&keywords=learning+linux+shell+scripting&qid=1734812639&sprefix=learning+linux+shell+scripting%2Caps%2C173&sr=8-6][1]


[1]: https://www.amazon.com/Learning-Linux-Shell-Scripting-real-world-ebook/dp/B07BJKGHGT/ref=sr_1_6?crid=1KU74JQO76FXR&dib=eyJ2IjoiMSJ9.J02cXEdwrtnKliTUyt_Z16dV3o_fKaDqHPlxrNp5bFImeqwnbu3cEzJfD3T9Bqc1x6A0iDYZERvjohkT4CkYY4xBNJ54gIxHAkqSbf0zUk-ByIOwLHsLm3_adNTTwpSCjxpoUDyglTRWlgNfimLz1X1A5WZ34VTvWsoLHiYBxjlEc9cyOs8Hl3DgMe65iBDCOYMhFJSkrXmruWH8R0DPXBzRM5iu10h_8GgGdm0nCNk.0xJGCPtqA2StVzJ27zJpuX78DfuUaPnff29dKN2aS0Y&dib_tag=se&keywords=learning+linux+shell+scripting&qid=1734812639&sprefix=learning+linux+shell+scripting%2Caps%2C173&sr=8-6
