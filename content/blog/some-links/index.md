---
title: Some links
date: "2023-06-21T14:29:37.121Z"
description: "Some links to other blogs."
---

[https://da8y01.gitlab.io/gl-blog/](https://da8y01.gitlab.io/gl-blog/)

[https://da8y01.github.io/gh-blog/](https://da8y01.github.io/gh-blog/)

[https://ring0a.blogspot.com/](https://ring0a.blogspot.com/)
