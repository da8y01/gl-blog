---
title: La vorágine - Jose Eustasio Rivera
date: "2025-01-01T18:08:12.121Z"
description: "El libro 'La vorágine' del autor colombiano Jose Eustasio Rivera."
tags: libro colombia LaVoragine_JoseEustasioRivera LaVoragine JoseEustasioRivera
---


![LaVoragine_JoseEustasioRivera_Cover](./image-placeholder-icon-7.jpg)


## <span id="PrimeraParte">[PRIMERA PARTE](#PrimeraParte)</span>
Arturo Cova y Alicia se encuentran en el departamento de Casanare como escapados de sus familias y la ciudad debido a las circunstancias de su relación amorosa, los acompaña Don Rafa, un conocedor del territorio, que más adelante los deja para irse a un viaje de negocios. Viajan por la inmensa y sublime llanura, pasan los días en algunas casas, cabañas, moriches, donde conocen a ciertas personas.
En una ocasión de licor y juego (dados) surge finalmente un altercado entre Cova y sus compañeros, y (el militar) Barrera y sus secuaces, hay disparos y demencia, Cova es herido (apuñalado) en un brazo, Barrera se retira a recuperarse en un sitio cercano, días después envía una carta a Cova.
Luego Cova, con complicidad de Clarita, libera la torada del negocio entre Zubieta y Barrera; después al tratar de reagrupar la torada con ayuda de la madrina de otras reses domadas sucede la tragedia de Millán, luego llegan a La Maporita, Zubieta muere, Griselda se ha llevado a Alicia, incendian la casa.


## <span id="SegundaParte">[SEGUNDA PARTE](#SegundaParte)</span>
Se enrumban a Vichada, licencian los caballos, el Pipa los relaciona con unos indios guahibo, se embarcan en una curiara en las oscuras aguas del río. Con los indios pasan un par de días mientras preparan el viaje remontando el río hacia Caviona, en la intemperie e inclemencia de la sequía y la inundación atraviesan el camino. Después se encaminan con indios maipureños, luego de mucho avanzar, en unos rápidos los indios maipureños junto con la canoa son absorbidos por un torbellino muriendo instantáneamente, el Pipa y los guahibos se fugan esa noche, Arturo Cova dice unas palabras duras a los cuatro que quedan (el mulato/moreno/indio Correa, Fidel Franco, Helí el Catire Mesa y Arturo Cova) para continuar. Conocen al pastuso viejo Clemente Silva quien les narra sus aventuras y exodos por la selva, ha caído en desgracia, busca a su pequeño hijo Luciano Silva, cuenta que conoce al señor Arana, viaja entre siringales, barracones y caucherías; conoce al sabio "monsiú" francés expedicionario, botánico, fotógrafo, éste se vuelve amigo y protector, se logra la publicación del artículo escrito por el periodista Saldaña Roca en el diario peruano "La Felpa" denunciando la situación en las caucherías; conoce al abuelo Balbino Jácome nativo de Garzón (Huila) que tiene una pierna seca por la mordedura de una tarántula, éste ejerce tareas de espía, ayuda a Clemente y a los trabajadores y nativos. Clemente logra conocer a la madona turca Zoraida Ayram, logra llegar a la ciudad de Iquitos, vagabundea buscando al Cónsul, al final de la narración de Clemente Silva se embarca hacia Manaos tras sus patrones que van en buque, por uno de los peones se entera de que su hijo ha muerto contra un árbol, en las raíces de un jacarandá.


## <span id="TerceraParte">[TERCERA PARTE](#TerceraParte)</span>
Despúes de su relato, en compañía del viejo Clemente Silva, los cinco continúan la travesía por la selva inhóspita en dirección a las barracas del Cayeno. Agachados escondidos atisbando las barracas que tenían por objetivo, se cuenta otra historia de andanza de don Clemente Silva como rumbero en la selva, esta vez como criado y siringuero, y la historia/anécdota de la fuga de la barraca, la huída del encuentro con las letales hormigas tambochas, pierden el camino, se escapan/evaden/evitan las hormigas tambochas en charcos, se dispersan para siempre, confiando en la guianza/seña de la palmera Clemente Silva encuentra el camino, es capturado por los Albuquerques, luego se fuga; ahora en el presente, con Arturo Cova, están expectantes ante las barracas que atisban. Acuerdan que sea Arturo Cova el que desarmado vaya a presentarse a la barraca, lo recibe el curtido General venezonalo Aquiles Vácares, luego llegan los otros compañeros de Cova, la madona Zoraida Ayram hace presencia, luego Cova intenta intimar con la madona, llegan los niños/indiecitos que por educación/adoctrinamiento temen al viejo Clemente Silva, ven chozas y el círculo de niñas madres, y a Esteban Ramírez/Ramiro Estévanez. Se refieren algunas calamidades vividas por Ramiro Estévanez, se menciona al tirano Coronel Funes, a Petardo Lesmes y al Gobernador Roberto Pulido, se planea el viaje en canoa a Manaos de Clemente Silva, el mulato Antonio Correa y otros para contactar al Cónsul y buscar que los liberten, Clemente se entera de que han botado los huesos de su hijo Luciano Silva, Arturo Cova empieza a pasar el tiempo escribiendo sus travesías. Se narra la noche de la matanza de San Fernando y su Gobernador Roberto Pulido a manos del Coronel Funes y sus secuaces. Se cuenta la forma en que la turca Zoraida Ayram ayudada por indios roba y desocupa los depósitos del Cayeno hacia su propio batelón, ven a la niña Griselda, Arturo le propone a Zoraida interrogar a Griselda, la entrevista se lleva a cabo, hablan, se menciona a Clarita que está con Funes y a Alicia que está con Barrera; el Cayeno llega/arriba y es derrotado, en un forcejeo al lanzarse al agua, lo esperan con carabinas y disparos, al tratar de huir sumergiéndose termina en una costa destripado con los perros siguiéndole el rastro fatal; luego al llegar/atracar/desembarcar en el barracón de Manuel Cardoso, Arturo encuentra a Barrera, luchan, finalmente Barrera es engullido por los caribes. Al final Alicia tiene al prematuro hijo sietemesino, huyendo de los apestados/enfermos el grupo remanente/que queda (al frente Arturo Cova con el bebé recién nacido, los navegantes Martel y Dólar detrás, y cerrando el grupo Franco y Helí cargando a Alicia) se interna con el objetivo de instalarse cerca para que el viejo don Clemente Silva pueda encontrarlos/hallarlos fácilmente. Al final el último cable del Cónsul al ministerio dice que Clemente ha estado buscando por meses infructuosamente.



## <span id="referencias">[REFERENCIAS](#referencias)</span>
* [https://es.wikipedia.org/wiki/La_vor%C3%A1gine][1]
* [https://es.wikipedia.org/wiki/Jos%C3%A9_Eustasio_Rivera][2]


[1]: https://es.wikipedia.org/wiki/La_vor%C3%A1gine
[2]: https://es.wikipedia.org/wiki/Jos%C3%A9_Eustasio_Rivera
