---
title: La conquista del pan - Piotr Kropotkin
date: "2023-11-06T16:14:37.121Z"
description: "El libro La conquista del pan del autor ruso Piotr Kropotkin."
tags: libro rusia pan PiotrKropotkin Kropotkin
---


![LaConquistaDelPan_PiotrKropotkin_Cover](./La_conquete_du_pain.jpg)


## <span id="referencias">[REFERENCIAS](#referencias)</span>
* [https://es.wikipedia.org/wiki/La_conquista_del_pan][1]
* [https://es.wikipedia.org/wiki/Piotr_Kropotkin][2]
* [https://proletarios.org/books/Kropotkin-La_Conquista_del_pan.pdf][3]
* [Kropotkin-La_Conquista_del_pan.pdf][4]
* Versión anotada con Adobe Acrobat XI: [Kropotkin-La_Conquista_del_pan_20230802.pdf][5]


[1]: https://es.wikipedia.org/wiki/La_conquista_del_pan
[2]: https://es.wikipedia.org/wiki/Piotr_Kropotkin
[3]: https://proletarios.org/books/Kropotkin-La_Conquista_del_pan.pdf
[4]: ./Kropotkin-La_Conquista_del_pan.pdf
[5]: ./Kropotkin-La_Conquista_del_pan_20230802.pdf
