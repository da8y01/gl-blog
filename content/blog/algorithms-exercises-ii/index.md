---
title: Three algorithms exercises - Part II
date: "2024-01-01T18:52:50.121Z"
description: "Second part of algorithms exercises from on-line code challenges."
tags: AlgorithmsExercises algorithms exercises CodeChallenge code programming
image: ./CoderlandCoderbyte.png
---


## <span id="SearchingChallenge">[Searching Challenge](#SearchingChallenge)</span>

![Coderbyte_2023-08-09_17_23_31](./Coderbyte_2023-08-09_17_23_31.png)

Have the function `SearchingChallenge(str)` take the `str` parameter being passed and find the longest substring that contains **k** unique characters, where **k** will be the first character from the string. The substring will start from the second position in the string because the first character will be the integer **k**. For example: if str is "2aabbacbaa" there are several substrings that all contain 2 unique characters, namely: ["aabba", "ac", "cb", "ba"], but your program should return "aabba" because it is the longest substring. If there are multiple longest substrings, then return the first substring encountered with the longest length. **k** will range from 1 to 6.

Taken from [https://www.geeksforgeeks.org/find-the-longest-substring-with-k-unique-characters-in-a-given-string/][5]

![geeksforgeeks1](./geeksforgeeks1.png)

![geeksforgeeks2](./geeksforgeeks2.png)

### [NodeJS][3] ([SearchingChallenge_2.js](./SearchingChallenge2_js.txt))
```javascript{numberLines: true}
// Javascript program to find the longest
// substring with k unique characters in
// a given string
let MAX_CHARS = 26;

// This function calculates number of
// unique characters using a associative
// array count[]. Returns true if no. of
// characters are less than required else
// returns false.
function isValid(count, k) {
  let val = 0;
  for (let i = 0; i < MAX_CHARS; i++) {
    if (count[i] > 0) {
      val++;
    }
  }

  // Return true if k is greater
  // than or equal to val
  return k >= val;
}

// Finds the maximum substring
// with exactly k unique chars
function kUniques(s, k) {
  // Number of unique characters
  let u = 0;
  let n = s.length;
  let count = new Array(MAX_CHARS);

  for (let i = 0; i < MAX_CHARS; i++) {
    count[i] = 0;
  }

  // Traverse the string, Fills
  // the associative array
  // count[] and count number
  // of unique characters
  for (let i = 0; i < n; i++) {
    if (count[s[i].charCodeAt(0) - "a".charCodeAt(0)] == 0) {
      u++;
    }
    count[s[i].charCodeAt(0) - "a".charCodeAt(0)]++;
  }

  // If there are not enough
  // unique characters, show
  // an error message.
  if (u < k) {
    // document.write("Not enough unique characters");
    console.log("Not enough unique characters");
    return;
  }

  // Otherwise take a window with
  // first element in it.
  // start and end variables.
  let curr_start = 0,
    curr_end = 0;

  // Also initialize values for
  // result longest window
  let max_window_size = 1;
  let max_window_start = 0;

  // Initialize associative
  // array count[] with zero
  for (let i = 0; i < MAX_CHARS; i++) {
    count[i] = 0;
  }

  // put the first character
  count[s[0].charCodeAt(0) - "a".charCodeAt(0)]++;

  // Start from the second character and add
  // characters in window according to above
  // explanation
  for (let i = 1; i < n; i++) {
    // Add the character 's[i]'
    // to current window
    count[s[i].charCodeAt(0) - "a".charCodeAt(0)]++;
    curr_end++;

    // If there are more than k
    // unique characters in
    // current window, remove from left side
    while (!isValid(count, k)) {
      count[s[curr_start].charCodeAt(0) - "a".charCodeAt(0)]--;
      curr_start++;
    }

    // Update the max window size if required
    if (curr_end - curr_start + 1 > max_window_size) {
      max_window_size = curr_end - curr_start + 1;
      max_window_start = curr_start;
    }
  }

  // document.write(
  //   "Max substring is : " +
  //     s.substring(max_window_start, max_window_start + max_window_size + 1) +
  //     " with length " +
  //     max_window_size
  // );
  console.log(
    "Max substring is : " +
      s.substring(max_window_start, max_window_start + max_window_size) +
      " with length " +
      max_window_size
  );
}

// Driver Code
// let s = "aabacbebebe";
// let k = 3;
let s = "aabbacbaa";
let k = 2;
kUniques(s, k);

// This code is contributed by rag2127
```

![SearchingChallenge_TerminalNode](./SearchingChallenge_TerminalNode.png)

### [Python][4] ([SearchingChallenge_1.py](./SearchingChallenge1_py.txt))
```python{numberLines: true}
# Python program to find the longest substring with k unique
# characters in a given string
MAX_CHARS = 26

# This function calculates number of unique characters
# using a associative array count[]. Returns true if
# no. of characters are less than required else returns
# false.
def isValid(count, k):
	val = 0
	for i in range(MAX_CHARS):
		if count[i] > 0:
			val += 1

	# Return true if k is greater than or equal to val
	return (k >= val)

# Finds the maximum substring with exactly k unique characters
def kUniques(s, k):
	u = 0 # number of unique characters
	n = len(s)

	# Associative array to store the count
	count = [0] * MAX_CHARS

	# Traverse the string, fills the associative array
	# count[] and count number of unique characters
	for i in range(n):
		if count[ord(s[i])-ord('a')] == 0:
			u += 1
		count[ord(s[i])-ord('a')] += 1

	# If there are not enough unique characters, show
	# an error message.
	if u < k:
		print ("Not enough unique characters")
		return

	# Otherwise take a window with first element in it.
	# start and end variables.
	curr_start = 0
	curr_end = 0

	# Also initialize values for result longest window
	max_window_size = 1
	max_window_start = 0

	# Initialize associative array count[] with zero
	count = [0] * len(count)

	count[ord(s[0])-ord('a')] += 1 # put the first character

	# Start from the second character and add
	# characters in window according to above
	# explanation
	for i in range(1,n):

		# Add the character 's[i]' to current window
		count[ord(s[i])-ord('a')] += 1
		curr_end+=1

		# If there are more than k unique characters in
		# current window, remove from left side
		while not isValid(count, k):
			count[ord(s[curr_start])-ord('a')] -= 1
			curr_start += 1

		# Update the max window size if required
		if curr_end-curr_start+1 > max_window_size:
			max_window_size = curr_end-curr_start+1
			max_window_start = curr_start

	print ("Max substring is : " + s[max_window_start:max_window_start + max_window_size]
	+ " with length " + str(max_window_size))

# Driver function
s = "aabacbebebe"
k = 3
# s = "aabbacbaa"
# k = 2
kUniques(s, k)

# This code is contributed by BHAVYA JAIN
```

![SearchingChallenge_TerminalPython](./SearchingChallenge_TerminalPython.png)


## <span id="Calculator">[Calculator](#Calculator)</span>

![Coderbyte_2023-08-15_16_40_17](./Coderbyte_2023-08-15_16_40_17.png)

Have the function `Calculator(str)` take the `str` parameter being passed and evaluate the mathematical expression within it. For example, if `str` were "2+(3-1)\*3" the output should be **8**. Another example: if `str` were "(2-0)(6/2)" the output should be **6**. There can be parenthesis within the string so you must evaluate it properly according to the rules of arithmetic. The string will contain the operators: +, -, /, \*, (, and ). If you have a string like this: #/#\*# or #+#(#)/#, then evaluate from left to right. So divide then multiply, and for the second one multiply, divide, then add. The evaluations will be such that there will not be any decimal operations, so you do not need to account for rounding and whatnot.


## <span id="HistogramArea">[Histogram Area](#HistogramArea)</span>

![Coderbyte_2023-08-15_17_36_16](./Coderbyte_2023-08-15_17_36_16.png)

Have the function `HistogramArea(str)` read the array of non-negative integers stored in `arr` which will represent the heights of bars on a graph (where each bar width is 1), and determine the largest area underneath the entire bar graph. For example: if `arr` is [2, 1, 3, 4, 1] then this looks like the following bar graph:
You can see in the above bar graph that the largest area underneath the graph is covered by the x's. The area of that space is equal to 6 because...

### [NodeJS][3] ([HistogramArea_1.js](./HistogramArea1_js.txt))
```javascript{numberLines: true}
function HistogramArea(arr) {
  let maxArea = 0
  const stack = []

  for (let i = 0; i < arr.length; i++) {
    while (stack.length > 0 && arr[i] < arr[stack[stack.length - 1]]) {
      const poppedIndex = stack.pop()
      const width = stack.length === 0 ? i : i - stack[stack.length - 1] - 1
      const area = arr[poppedIndex] * width
      maxArea = Math.max(maxArea, area)
    }
    stack.push(i)
  }

  while (stack.length > 0) {
    const poppedIndex = stack.pop()
    const width = stack.length === 0 ? arr.length : arr.length - stack[stack.length - 1] - 1
    const area = arr[poppedIndex] * width
    maxArea = Math.max(maxArea, area)
  }

  return maxArea
}

console.log(HistogramArea([2, 1, 3, 4, 1]))
```

![HistogramArea_TerminalNode](./HistogramArea_TerminalNode.png)

### [Python][4] ([HistogramArea_1.py](./HistogramArea1_py.txt))
```python{numberLines: true}
def HistogramArea(arr):
    maxArea = 0
    stack = []

    for i in range(len(arr)):
        while len(stack) > 0 and arr[i] < arr[stack[len(stack) - 1]]:
            poppedIndex = stack.pop(-1)
            width = i if len(stack) == 0 else i - stack[len(stack) - 1] - 1
            area = arr[poppedIndex] * width
            maxArea = max(maxArea, area)
        stack.append(i)

    while len(stack) > 0:
        poppedIndex = stack.pop(-1)
        width = len(arr) if len(stack) == 0 else len(
            arr) - stack[len(stack) - 1] - 1
        area = arr[poppedIndex] * width
        maxArea = max(maxArea, area)

    return maxArea

print(HistogramArea([2, 1, 3, 4, 1]))
```

![HistogramArea_TerminalPython](./HistogramArea_TerminalPython.png)


## <span id="references">[REFERENCES](#references)</span>
* [https://www.coderland.com/en][1]
* [https://coderbyte.com/][2]
* [https://nodejs.org/en][3]
* [https://www.python.org/][4]
* [https://www.geeksforgeeks.org/find-the-longest-substring-with-k-unique-characters-in-a-given-string/][5]


[1]: https://www.coderland.com/en
[2]: https://coderbyte.com/
[3]: https://nodejs.org/en
[4]: https://www.python.org/
[5]: https://www.geeksforgeeks.org/find-the-longest-substring-with-k-unique-characters-in-a-given-string/
