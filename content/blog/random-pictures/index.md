---
title: Random pictures
date: "2023-07-03T10:17:37.121Z"
description: "Some random pictures."
---

<div style="text-align:center">
    <img src="./ConceptCoastlinesFractalDimension_320x320.gif" alt="ConceptCoastlinesFractalDimension_320x320" />
</div>

![DJCat_300x300](./DJCat_300x300.png)

![HAL9000_800x800](./HAL9000_800x800.png)

<div style="text-align:center">
    <video src="https://gitlab.com/da8y01/gl-blog/-/raw/main/custom/MandelbrotSet_320x320.mp4" controls width="320" height="320">
        Your browser does not support the video tag.
    </video>
</div>

![NeromancerCover_400x400](./NeromancerCover_400x400.jpg)

<div style="text-align:center">
    <img src="./ObrasInf_113x113.gif" alt="ObrasInf_113x113" />
</div>

![PiBanner_916x480](./PiBanner_916x480.jpeg)

![SauronEye_1024x632](./SauronEye_1024x632.jpg)

<div style="text-align:center">
    <img src="./StarWars_100x40.gif" alt="StarWars_100x40" />
</div>

![StaticNoise_800x600](./StaticNoise_800x600.jpg)

<video src="https://gitlab.com/da8y01/gl-blog/-/raw/main/custom/StreetFighter.mp4" controls>
    Your browser does not support the video tag.
</video>

![YinYang_250x250](./YinYang_250x250.png)
